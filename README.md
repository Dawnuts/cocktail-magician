# Cocktail Magician Project Documentation

# Created By
- Ivan Ladzhev
- Ginka Spasova

# What is Cocktail Magician?
Cocktail Magician is a web application for exploring recipes for innovative, exotic cocktails and follows their distribution and success in amazing bars. The application enables the users to see bars and the cocktails they offer, rate them and leave a comment for them, sort and filter them by different criteria. Given the appropriate cocktail magician access, they can also create, edit, delete or restore bars or cocktails. They can also mix the ingredients of their favourite cocktails as well as create new tasty ingredients.

# Description of the main parts and functionalities of the project
## Public part
The public part is visible for all website visitors and doesn`t require authentication. It consists of a home page, displaying top rated bars and cocktails, Login and Register page.
![](Images/homepage.PNG)
![](Images/homepage-2.PNG)

Upon clicking a bar or cocktail, the visitor can see details about the bar
 - image
-  name
- rating
- address
- phone
- comments 
- the cocktails it offers – with links to the cocktail
![](Images/bar-details.PNG)

And details about the cocktail
- image
- name
- description
- rating
- comments 
- cocktail ingredients
![](Images/cocktail-details.PNG)

Visitors can also explore all bars and cocktails, listed in the website, sort or filter them by different criteria.
![](Images/cocktails-index.PNG)

#

## Private part
The private part is accessible only after authentication. After login, users can rate bar or cocktail with 1 to 5 stars.

![](Images/rating.PNG)

Users can also leave a comment for bar or cocktail.
![](Images/comment.PNG)

#
## Administration part
The administration part is accessible for admins only. They can:
- See and manage all ingredients
![](Images/ingredients-index.PNG)
![](Images/ingredient-details.PNG)

- Mix the ingredients of their favorite cocktails or add new ones.

![](Images/mix-it-up.PNG)

- Manage bars and cocktails.

![](Images/edit-cocktail.PNG)

- Manage users – ban or unban them.
![](Images/users.PNG)

#
# Database Schema
![](Images/database-diagram.PNG)