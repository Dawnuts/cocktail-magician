﻿using CocktailMagician.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Extensions
{
    public static class DataSeeder
    {
        public static void SeedData(this ModelBuilder builder)
        {
            //Admins
            builder.Entity<Role>().HasData
          (
              new Role
              {
                  Id = Guid.Parse("469690a1-0da5-413d-b82f-f9ec5d96502d"),
                  Name = "Admin",
                  NormalizedName = "ADMIN",
                  ConcurrencyStamp = Guid.NewGuid().ToString()
              },
              new Role
              {
                  Id = Guid.Parse("1d195e6d-80cf-47eb-a840-2ddeb11734c9"),
                  Name = "Member",
                  NormalizedName = "MEMBER",
                  ConcurrencyStamp = Guid.NewGuid().ToString()
              }
          );

            var hasher = new PasswordHasher<User>();

            var user1 = new User
            {
                Id = Guid.Parse("25b9ec71-4db2-489e-9d55-a7a0a2b881e6"),
                UserName = "gandalf@cm.com",
                Email = "gandalf@cm.com",
                NormalizedEmail = "GANDALF@CM.COM",
                SecurityStamp = "15CLJEKQCTLPRXMVXXNSWXZH6R6KJRRU",
                CreatedOn = DateTime.Now,
                NormalizedUserName = "GANDALF@CM.COM",
            };
            user1.PasswordHash = hasher.HashPassword(user1, "g@ndalf123");
            var user2 = new User
            {
                Id = Guid.Parse("c39cbf0f-106c-426a-b3c1-f5fe94ada252"),
                UserName = "harrypotter@cm.com",
                Email = "harrypotter@cm.com",
                NormalizedEmail = "HARRYPOTTER@CM.COM",
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                CreatedOn = DateTime.Now,
                NormalizedUserName = "HARRYPOTTER@CM.COM",

            };
            user2.PasswordHash = hasher.HashPassword(user2, "h@rry123");
            builder.Entity<User>().HasData(
                user1, user2
               );
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("469690a1-0da5-413d-b82f-f9ec5d96502d"),
                    UserId = user1.Id
                },
                  new IdentityUserRole<Guid>
                  {
                      RoleId = Guid.Parse("469690a1-0da5-413d-b82f-f9ec5d96502d"),
                      UserId = user2.Id
                  }
                );
            //Ingredients

            var whisky = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Whisky",
                ImgPath = "/images/ingredients/whiskey.jpg"
            };

            var angostura = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Angostura bitters",
                ImgPath = "/images/ingredients/angostura-bitters.jpg"
            };

            var brownSugar = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Brown sugar",
                ImgPath = "/images/ingredients/brown-sugar.jpeg"
            };

            var soda = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Soda water",
                ImgPath = "/images/ingredients/soda-water.jpg"
            };
            var orangeSlices = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Orange slices",
                ImgPath = "/images/ingredients/orange-slices.jpg"
            };
            var whiteRum = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "White rum",
                ImgPath = "/images/ingredients/white-rum.jpg"
            };
            var limeJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Lime juice",
                ImgPath = "/images/ingredients/lime-juice.jpg"
            };
            var mint = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Mint",
                ImgPath = "/images/ingredients/mint.jpg"
            };
            var vodka = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Vodka",
                ImgPath = "/images/ingredients/vodka.jpg"
            };
            var tripleSec = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Triple sec",
                ImgPath = "/images/ingredients/triple-sec.jpg"
            };
            var cranberryJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Cranberry juice",
                ImgPath = "/images/ingredients/cranberry-juice.jpg"
            };
            var vermouth = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Vermouth",
                ImgPath = "/images/ingredients/vermouth.jpg"
            };
            var lemonJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Lemon juice",
                ImgPath = "/images/ingredients/lemon-juice.jpg"
            };
            var strawberry = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Strawberry",
                ImgPath = "/images/ingredients/strawberry.jpg"
            };
            var sugar = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Sugar",
                ImgPath = "/images/ingredients/sugar.jpg"
            };
            var salt = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Salt",
                ImgPath = "/images/ingredients/salt.jpg"
            };
            var jack = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Jack Daniels",
                ImgPath = "/images/ingredients/jack-daniels.png"
            };
            var cola = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Coca Cola",
                ImgPath = "/images/ingredients/coca-cola.jpg"
            };
            var cream = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Cream",
                ImgPath = "/images/ingredients/cream.jpg"
            };
            var pineapleJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Pineaple juice",
                ImgPath = "/images/ingredients/pineapple-juice.jpg"
            };
            var gin = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Gin",
                ImgPath = "/images/ingredients/gin.jpg"
            };
            var tequila = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Tequila",
                ImgPath = "/images/ingredients/tequila.jpeg"
            };
            var orangeJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Orange juice",
                ImgPath = "/images/ingredients/orange-juice.jpg"
            };
            var grenadine = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Grenadine syrup",
                ImgPath = "/images/ingredients/grenadine.jpg"
            };
            var sugarSyrup = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Sugar syrup",
                ImgPath = "/images/ingredients/sugar-syrup.jpg"
            };
            var peachSchnapps = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Peach Schnapps",
                ImgPath = "/images/ingredients/peach-schnapps.jpg"
            };
            var coffeeLiqueur = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Coffee Liqueur",
                ImgPath = "/images/ingredients/coffee-liqueur.jpg"
            };

            var worcestershire = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Worcestershire Sauce",
                ImgPath = "/images/ingredients/worcestershire.jpg"
            };

            var tabasco = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Tabasco",
                ImgPath = "/images/ingredients/tabasco.jpg"
            };
            var tomatoJuice = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Tomato juice",
                ImgPath = "/images/ingredients/tomatoJuice.jpg"
            };

            builder.Entity<Ingredient>().HasData(whisky, angostura, brownSugar, soda, orangeSlices, whiteRum, limeJuice, mint,
                vodka, tripleSec, cranberryJuice, vermouth, lemonJuice, strawberry, sugar, salt, jack, cola, cream, pineapleJuice,
                gin, tequila, orangeJuice, grenadine, sugarSyrup, peachSchnapps, coffeeLiqueur, worcestershire, tabasco, tomatoJuice);

            //Cocktails
            var bloodyMary = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Bloody Mary",
                Description = "The Bloody Mary is a vodka-soaked nutritional breakfast and hangover cure all in one. There's a reason this iconic cocktail is a classic - you really have to try it.",
                ImgPath = "/images/cocktails/bloody-mary.jpg"
            };
            var oldFashioned = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Old fashioned",
                Description = "A classic cocktail, the Old Fashioned will always be a delicious bourbon drink that never goes out of style.",
                ImgPath = "/images/cocktails/oldfashioned.jpg"
            };
            var mojito = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Mojito",
                Description = "It's one of the most refreshing drinks you can mix up. Its combination of sweetness, citrus, and herbaceous mint flavors is intended to complement the rum, and has made the mojito a popular summer drink",
                ImgPath = "/images/cocktails/mojito.jpg"
            };
            var cosmopolitan = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cosmopolitan",
                Description = "Lipsmackingly sweet-and-sour, the Cosmopolitan cocktail of vodka, cranberry, orange liqueur and citrus is a good time in a glass. Perfect for a party.",
                ImgPath = "/images/cocktails/cosmopolitan.jpg"
            };
            var manhattan = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Manhattan",
                Description = "The Manhattan’s mix of American whiskey and Italian vermouth, perked up with a few dashes of aromatic bitters, is timeless and tasty—the very definition of what a cocktail should be.",
                ImgPath = "/images/cocktails/manhattan.jpg"
            };
            var strawberryDaiquiri = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Strawberry daiquiri",
                Description = "The Strawberry Daiquiri is a classic summertime drink. Chill down with this frosty rum concoction.",
                ImgPath = "/images/cocktails/strawberry-daiquiri.jpg"
            };
            var margarita = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Margarita",
                Description = "Served straight up or on the rocks, the margarita is one of the most popular cocktails of all time. And for good reason! It will cool you down on a hot day or warm you up on a cool day. Any day is a good day for a margarita.",
                ImgPath = "/images/cocktails/margarita.jpg"
            };
            var jackncoke = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Jack'n'Coke",
                Description = "The Jack and Coke is a classic, easygoing cocktail that blends Jack Daniels whiskey with cola. When smooth, charcoal-mellowed Tennessee Whiskey first met the sweet fizz of Coke, this classic cocktail was born.",
                ImgPath = "/images/cocktails/jack-and-coke.jpg"
            };
            var cubaLibre = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba libre",
                Description = "The Cuba Libre is a refreshing and incredibly simple mixed drink. It is similar to a rum and Coke, but there is a twist you will not want to miss.",
                ImgPath = "/images/cocktails/cuba-libre.jpg"
            };
            var pinaColada = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Pina Colada",
                Description = "A tropical blend of rich coconut cream, white rum and tangy pineapple - served with an umbrella for kitsch appeal.",
                ImgPath = "/images/cocktails/pina-colada.jpg"
            };
            var martini = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Martini",
                Description = "Of all the classic cocktails in all the cocktail bars in all the world, the iconic Martini reigns supreme. Stunning in its simplicity and strikingly elegant when ordered with confidence, a perfect Martini showcases the sharp, botanical flavors of gin and vermouth with little to no interference.",
                ImgPath = "/images/cocktails/martini.jpg"
            };
            var tequilaSunrise = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Tequila Sunrise",
                Description = "The Tequila Sunrise, with its bright striations of color, evokes a summer sunrise. This simple classic has only three ingredients—tequila, grenadine and orange juice—and is served unmixed to preserve the color of each layer.",
                ImgPath = "/images/cocktails/tequila-sunrise.jpg"
            };
            var daiquiri = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Classic daiquiri",
                Description = "The daiquiri is a classic rum sour drink. It is also one of the freshest drinks you can find and an essential rum cocktail everyone should know and taste. ",
                ImgPath = "/images/cocktails/daiquiri.jpg"
            };
            var sexOnTheBeach = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Sex on the beach",
                Description = "The perfect summer cocktail to enjoy by the beach but tasty enough to drink all year round. Combine vodka with peach schnapps and cranberry juice to make a classic sex on the beach cocktail. Garnish with cocktail cherries and orange slices.",
                ImgPath = "/images/cocktails/sex-on-the-beach.jpg"
            };
            var whiteRussian = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "White russian",
                Description = "A White Russian cocktail is a fall and winter delight. Made with a coffee liqeur like Kahlua with milk or cream, it's a delicious sweet dessert drink. ",
                ImgPath = "/images/cocktails/white-russian.jpg"
            };
            var blackRussian = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Black russian",
                Description = "It takes a good vodka drink to survive a Moscow winter, and the classic Black Russian cocktail definitely makes the cut.",
                ImgPath = "/images/cocktails/black-russian.jpg"
            };
            var longIslandIcedTea = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Long Island iced tea",
                Description = "On paper, the Long Island Iced Tea is one hot mess of a drink. Four different—and disparate—spirits slugging it out in a single glass, along with triple sec, lemon juice and cola? The recipe reads more like a frat house hazing than one of the world’s most popular cocktails. And yet, somehow, it works.",
                ImgPath = "/images/cocktails/long-island-iced-tea.jpg"
            };
            builder.Entity<Cocktail>().HasData(bloodyMary, oldFashioned, mojito, cosmopolitan, manhattan,
                strawberryDaiquiri, margarita, jackncoke, cubaLibre, pinaColada, martini, tequilaSunrise, daiquiri, sexOnTheBeach,
                whiteRussian, blackRussian, longIslandIcedTea);

            //Bars
            var fuzzyBar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Fuzzy bar",
                Description = "Fuzzy is a cozy bar in the heart of the city with small stage for acoustic music, magic, comedy and other live events. It has it all - cocktails, craft beer and all kind of alcohol mixed with good music and vibe.",
                Address = "Sofia 1164, Nerazdelni 14",
                Phone = "088 349 3517",
                ImgPath = "/images/bars/fuzzy-bar.jpg"
            };
            var rockNRolla = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Rock'n'Rolla",
                Description = "The home of good music and mood in Sofia has one name: Rock'n'Rolla. This bar and club is the best place to go if you love rock music and want to mingle with a crowd of friendly similarly-minded people.",
                Address = "Sofia 1000, Graf Ignatiev 1",
                Phone = "088 913 1344",
                ImgPath = "/images/bars/rock-n-rolla.jpg"
            };
            var daiquiriBar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Daiquiri Cocktail bar",
                Description = "The Team Daiquiri bartenders adds acrobatic flair to mixology by juggling glasses and bottles while creating legendary Cocktails every night of the week. The bar proudly serves all sorts of tropical cocktails, frozen margaritas, and a long list of liqueurs, beers, shots and Specials.",
                Address = "Golden Sands 9007, Mimoza Hotel",
                Phone = "088 580 0275",
                ImgPath = "/images/bars/daiquiri-cocktail-bar.jpg"
            };
            var bedroomBeachClub = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Bedroom beach club",
                Description = "An extravagant beach and night club, associated with high class clubbing entertainment. Bedroom beach is characterized by an elegant interior, made out of natural materials, stylish extended space, interesting inner architecture and decor, impressive electric boutique L-acoustic sound, first-class service and the most modern lifestyle mood that can be felt only by passing its entrance.",
                Address = "Sunny Beach 8240",
                Phone = "088 886 0666",
                ImgPath = "/images/bars/bedroom-beach-club.jpg"
            };
            var carrusel = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Carrusel club",
                Description = "The best night club in Sofia, located in the heart of the city. The club offers cozy atmosphere, very good service, very competitive prices and a very good quality of music. International DJ's are listed at least 4 times per month.",
                Address = "Sofia 1000, Georgi Sava Rakovski 108",
                Phone = "088 996 9687",
                ImgPath = "/images/bars/carrusel-club.jpg"
            };
            var buddys = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Buddy's",
                Description = "It is so simply surrounded by greenery and wherever you look you see friends!",
                Address = "Plovdiv 4000, Captain Raicho 50",
                Phone = "089 944 5356",
                ImgPath = "/images/bars/default-bar.jpeg"
            };
            var noSense = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "No sense",
                Description = "A place of mystery where no night is the same. The music as well as the people can surprise you at any moment. The only thing that's promised is the fun.",
                Address = "Plovdiv 4000, Evlogi Georgiev 5",
                Phone = "089 673 8654",
                ImgPath = "/images/bars/no-sense.jpg"
            };
            var mixtape = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "MIXTAPE 5",
                Description = "Club *MIXTAPE 5* is one of the most popular party and concert venues in Sofia. It is located in the city central area, close to the National Palace of Culture. Since 2011 the club has hosted hundreds of live music events, DJ parties and performances featuring both local and foreign artists.",
                Address = "Sofia 1000, Blvd Bulgaria 1",
                Phone = "087 929 5965",
                ImgPath = "/images/bars/mixtape.jpg"
            };
            var tomato = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Tomato",
                Description = "Great atmosphere, and relaxing music, a place to forget about bad things, take a break and enjoy life.",
                Address = "Plovdiv 4000, Blvd  6th of September 163",
                Phone = "089 637 0777",
                ImgPath = "/images/bars/tomato.jpg"
            };
            var rockIt = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt",
                Description = "RockIT Rock Bar is one of the best rock and metal music venues in Sofia. The bar has different halls - central with a stage where lifetime performances and DJ parties take place, and gambling halls with billiards, table football and other entertainment, interesting cuisine, beer appetizers and tempting desserts!",
                Address = "Sofia 1408, Petko Karavelov 5",
                Phone = "088 866 6991",
                ImgPath = "/images/bars/rockit.jpg"
            };
            builder.Entity<Bar>().HasData(fuzzyBar, rockNRolla, daiquiriBar, bedroomBeachClub, carrusel, buddys, noSense,
                mixtape, tomato, rockIt);

            //CocktailIngredients
            builder.Entity<CocktailIngredient>().HasData(
                new CocktailIngredient { CocktailId = bloodyMary.Id, IngredientId = vodka.Id },
                new CocktailIngredient { CocktailId = bloodyMary.Id, IngredientId = tomatoJuice.Id },
                new CocktailIngredient { CocktailId = bloodyMary.Id, IngredientId = worcestershire.Id },
                new CocktailIngredient { CocktailId = bloodyMary.Id, IngredientId = tabasco.Id },
                new CocktailIngredient { CocktailId = bloodyMary.Id, IngredientId = lemonJuice.Id },
                new CocktailIngredient { CocktailId = oldFashioned.Id, IngredientId = whisky.Id },
                new CocktailIngredient { CocktailId = oldFashioned.Id, IngredientId = angostura.Id},
                new CocktailIngredient { CocktailId = oldFashioned.Id, IngredientId = brownSugar.Id},
                new CocktailIngredient { CocktailId = oldFashioned.Id, IngredientId = soda.Id},
                new CocktailIngredient { CocktailId = oldFashioned.Id, IngredientId = orangeSlices.Id},
                new CocktailIngredient { CocktailId = mojito.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = mojito.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = mojito.Id, IngredientId = mint.Id},
                new CocktailIngredient { CocktailId = mojito.Id, IngredientId = brownSugar.Id},
                new CocktailIngredient { CocktailId = mojito.Id, IngredientId = soda.Id},
                new CocktailIngredient { CocktailId = cosmopolitan.Id, IngredientId = vodka.Id},
                new CocktailIngredient { CocktailId = cosmopolitan.Id, IngredientId = tripleSec.Id},
                new CocktailIngredient { CocktailId = cosmopolitan.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = cosmopolitan.Id, IngredientId = cranberryJuice.Id},
                new CocktailIngredient { CocktailId = manhattan.Id, IngredientId = whisky.Id},
                new CocktailIngredient { CocktailId = manhattan.Id, IngredientId = angostura.Id},
                new CocktailIngredient { CocktailId = manhattan.Id, IngredientId = vermouth.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = lemonJuice.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = strawberry.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = sugar.Id},
                new CocktailIngredient { CocktailId = strawberryDaiquiri.Id, IngredientId = soda.Id},
                new CocktailIngredient { CocktailId = margarita.Id, IngredientId = tequila.Id},
                new CocktailIngredient { CocktailId = margarita.Id, IngredientId = tripleSec.Id},
                new CocktailIngredient { CocktailId = margarita.Id, IngredientId = salt.Id},
                new CocktailIngredient { CocktailId = margarita.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = jackncoke.Id, IngredientId = jack.Id},
                new CocktailIngredient { CocktailId = jackncoke.Id, IngredientId = cola.Id},
                new CocktailIngredient { CocktailId = cubaLibre.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = cubaLibre.Id, IngredientId = cola.Id},
                new CocktailIngredient { CocktailId = cubaLibre.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = pinaColada.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = pinaColada.Id, IngredientId = cream.Id},
                new CocktailIngredient { CocktailId = pinaColada.Id, IngredientId = pineapleJuice.Id},
                new CocktailIngredient { CocktailId = martini.Id, IngredientId = gin.Id},
                new CocktailIngredient { CocktailId = martini.Id, IngredientId = vermouth.Id},
                new CocktailIngredient { CocktailId = tequilaSunrise.Id, IngredientId = tequila.Id},
                new CocktailIngredient { CocktailId = tequilaSunrise.Id, IngredientId = orangeJuice.Id},
                new CocktailIngredient { CocktailId = tequilaSunrise.Id, IngredientId = grenadine.Id},
                new CocktailIngredient { CocktailId = daiquiri.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = daiquiri.Id, IngredientId = limeJuice.Id},
                new CocktailIngredient { CocktailId = daiquiri.Id, IngredientId = sugarSyrup.Id},
                new CocktailIngredient { CocktailId = sexOnTheBeach.Id, IngredientId = vodka.Id},
                new CocktailIngredient { CocktailId = sexOnTheBeach.Id, IngredientId = cranberryJuice.Id},
                new CocktailIngredient { CocktailId = sexOnTheBeach.Id, IngredientId = peachSchnapps.Id},
                new CocktailIngredient { CocktailId = sexOnTheBeach.Id, IngredientId = orangeSlices.Id},
                new CocktailIngredient { CocktailId = whiteRussian.Id, IngredientId = vodka.Id},
                new CocktailIngredient { CocktailId = whiteRussian.Id, IngredientId = coffeeLiqueur.Id},
                new CocktailIngredient { CocktailId = whiteRussian.Id, IngredientId = cream.Id},
                new CocktailIngredient { CocktailId = blackRussian.Id, IngredientId = vodka.Id},
                new CocktailIngredient { CocktailId = blackRussian.Id, IngredientId = coffeeLiqueur.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = vodka.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = whiteRum.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = tequila.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = gin.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = tripleSec.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = lemonJuice.Id},
                new CocktailIngredient { CocktailId = longIslandIcedTea.Id, IngredientId = cola.Id}
                );

            builder.Entity<BarCocktail>().HasData(
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = strawberryDaiquiri.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = margarita.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = longIslandIcedTea.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = whiteRussian.Id},
                new BarCocktail { BarId = fuzzyBar.Id, CocktailId = blackRussian.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = blackRussian.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = whiteRussian.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = oldFashioned.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = jackncoke.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = manhattan.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = tequilaSunrise.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = martini.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = daiquiri.Id},
                new BarCocktail { BarId = rockNRolla.Id, CocktailId = strawberryDaiquiri.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = strawberryDaiquiri.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = daiquiri.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = tequilaSunrise.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = mojito.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = daiquiriBar.Id, CocktailId = pinaColada.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = pinaColada.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = longIslandIcedTea.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = daiquiri.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = mojito.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = margarita.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = tequilaSunrise.Id},
                new BarCocktail { BarId = bedroomBeachClub.Id, CocktailId = martini.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = martini.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = manhattan.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = oldFashioned.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = jackncoke.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = whiteRussian.Id},
                new BarCocktail { BarId = carrusel.Id, CocktailId = blackRussian.Id},
                new BarCocktail { BarId = buddys.Id, CocktailId = mojito.Id},
                new BarCocktail { BarId = buddys.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = buddys.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = jackncoke.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = tequilaSunrise.Id},
                new BarCocktail { BarId = noSense.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = manhattan.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = whiteRussian.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = blackRussian.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = jackncoke.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = martini.Id},
                new BarCocktail { BarId = mixtape.Id, CocktailId = oldFashioned.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = oldFashioned.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = bloodyMary.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = mojito.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = pinaColada.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = whiteRussian.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = blackRussian.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = tequilaSunrise.Id},
                new BarCocktail { BarId = tomato.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = sexOnTheBeach.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = manhattan.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = daiquiri.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = strawberryDaiquiri.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = jackncoke.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = cubaLibre.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = longIslandIcedTea.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = martini.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = pinaColada.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = margarita.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = cosmopolitan.Id},
                new BarCocktail { BarId = rockIt.Id, CocktailId = bloodyMary.Id}
                );

        }
    }
}
