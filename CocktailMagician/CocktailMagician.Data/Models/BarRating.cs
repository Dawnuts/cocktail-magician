﻿using System;

namespace CocktailMagician.Data.Models
{
    public class BarRating
    {
        public Guid BarId { get; set; }

        public Bar Bar { get; set; }

        public int Rating { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}