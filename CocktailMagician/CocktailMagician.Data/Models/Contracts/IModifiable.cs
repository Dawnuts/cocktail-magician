﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Models.Contracts
{
    interface IModifiable
    {
        public DateTime? ModifiedOn { get; set; }
    }
}
