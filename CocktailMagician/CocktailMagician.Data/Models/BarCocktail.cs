﻿using CocktailMagician.Data.Models.Contracts;
using System;

namespace CocktailMagician.Data.Models
{
    public class BarCocktail : IDeletable
    {
        public Guid CocktailId { get; set; }

        public Cocktail Cocktail { get; set; }

        public Guid BarId { get; set; }

        public Bar Bar { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}