﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CocktailMagician.Data.Models
{
    public class CocktailComment
    {
        public Guid Id { get; set; }
        public Guid CocktailId { get; set; }
        
        public Cocktail Cocktail { get; set; }
        [StringLength(500)]
        public string Text { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}