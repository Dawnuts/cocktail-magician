﻿using CocktailMagician.Data.Models.Contracts;
using System;

namespace CocktailMagician.Data.Models
{
    public class CocktailIngredient :IDeletable
    {
        public Guid IngredientId { get; set; }

        public Ingredient Ingredient { get; set; }

        public Guid CocktailId { get; set; }

        public Cocktail Cocktail { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}