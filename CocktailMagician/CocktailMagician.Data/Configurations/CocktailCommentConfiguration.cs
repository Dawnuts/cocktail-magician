﻿using CocktailMagician.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Configurations
{
    class CocktailCommentConfiguration : IEntityTypeConfiguration<CocktailComment>
    {
        public void Configure(EntityTypeBuilder<CocktailComment> builder)
        {
            builder.HasKey(comment => comment.Id);

            builder.Property(comment => comment.CocktailId)
                .IsRequired();
            builder.Property(comment => comment.UserId)
             .IsRequired();

            builder.Property(comment => comment.Text)
                .IsRequired();
        }
    }
}
