﻿using CocktailMagician.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Configurations
{
    public class BarConfiguration : IEntityTypeConfiguration<Bar>
    {
        public void Configure(EntityTypeBuilder<Bar> builder)
        {
            builder.HasKey(bar => bar.Id);

            builder.Property(bar => bar.Name)
                .IsRequired();

            builder.Property(bar => bar.Description)
                .IsRequired();

            builder.Property(bar => bar.Address)
                .IsRequired();

            builder.Property(bar => bar.Phone)
                .IsRequired();

            builder.Property(bar => bar.ImgPath);
            
            builder.HasMany(bar => bar.Cocktails)
                .WithOne(cocktail => cocktail.Bar);

            builder.HasMany(bar => bar.Comments)
                .WithOne(comment => comment.Bar);

            builder.HasMany(bar => bar.Ratings)
                .WithOne(rating => rating.Bar);
        }
    }
}
