﻿using CocktailMagician.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Data.Configurations
{
    public class CocktailConfiguration : IEntityTypeConfiguration<Cocktail>
    {
        public void Configure(EntityTypeBuilder<Cocktail> builder)
        {
            builder.HasKey(cocktail => cocktail.Id);

            builder.Property(cocktail => cocktail.Name)
                .IsRequired();

            builder.Property(cocktail => cocktail.Description)
                .IsRequired();

            builder.Property(cocktail => cocktail.ImgPath);

            builder.HasMany(cocktail => cocktail.Ingredients)
               .WithOne(cocktailIngredient => cocktailIngredient.Cocktail);

            builder.HasMany(cocktail => cocktail.Bars)
                .WithOne(b => b.Cocktail);

            builder.HasMany(cocktail => cocktail.Ratings)
                .WithOne(rating => rating.Cocktail);

            builder.HasMany(cocktail => cocktail.Comments)
                .WithOne(comment => comment.Cocktail);
        }
    }
}
