﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class CocktailCommentService : ICocktailCommentService
    {
        private readonly CMDbContext dbContext;
        private readonly IFactory factory;
        private readonly IDTOMapper<CocktailComment, CocktailCommentDTO> mapper;

        public CocktailCommentService(CMDbContext dbContext, IFactory factory, IDTOMapper<CocktailComment, CocktailCommentDTO> mapper)
        {
            this.dbContext = dbContext;
            this.factory = factory;
            this.mapper = mapper;
        }
        public async Task<CocktailCommentDTO> CreateCocktailCommentAsync(CocktailCommentDTO newCocktailComment)
        {
            if (newCocktailComment == null)
            {
                throw new ArgumentNullException();
            }

            var cocktailComment = this.factory.CreateCocktailComment(newCocktailComment.Text);
            cocktailComment.Id = newCocktailComment.Id;
            cocktailComment.UserId = newCocktailComment.UserId;
            cocktailComment.CocktailId = newCocktailComment.CocktailId;

            await this.dbContext.CocktailComments.AddAsync(cocktailComment);
            await this.dbContext.SaveChangesAsync();

            return newCocktailComment;
        }

        public async Task<bool> DeleteCocktailCommentAsync(Guid id)
        {
            var cocktailComment = await this.dbContext.CocktailComments
               .FirstOrDefaultAsync(cc => cc.Id == id);

            if (cocktailComment == null)
            {
                return false;
            }

            this.dbContext.CocktailComments.Remove(cocktailComment);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<CocktailCommentDTO> EditCocktailCommentAsync(Guid id, string newComment)
        {
            var cocktailComment = await this.dbContext.CocktailComments
                   .FirstOrDefaultAsync(cc => cc.Id == id);

            if (cocktailComment == null)
            {
                throw new ArgumentNullException();
            }

            cocktailComment.Text = newComment;

            this.dbContext.CocktailComments.Update(cocktailComment);
            await this.dbContext.SaveChangesAsync();

            var cocktailCommentDTO = this.mapper.MapToDTO(cocktailComment);

            return cocktailCommentDTO;
        }
    }
}
