﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.DTOs
{
    public class BarDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgPath { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public ICollection<CocktailDTO> Cocktails { get; set; } = new List<CocktailDTO>();
        public ICollection<BarCommentDTO> Comments { get; set; } = new List<BarCommentDTO>();
        public ICollection<BarRatingDTO> Ratings { get; set; }
        public double AverageRating { get; set; }
    }
}
