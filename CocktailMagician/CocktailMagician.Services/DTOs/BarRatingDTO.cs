﻿using System;

namespace CocktailMagician.Services.DTOs
{
    public class BarRatingDTO
    {
        public Guid BarId { get; set; }
        public string Bar { get; set; }
        public int Rating { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}