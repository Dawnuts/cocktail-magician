﻿using System;

namespace CocktailMagician.Services.DTOs
{
    public class BarCommentDTO
    {
        public Guid Id { get; set; }
        public Guid BarId { get; set; }
        public string Bar { get; set; }
        public string Text { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}