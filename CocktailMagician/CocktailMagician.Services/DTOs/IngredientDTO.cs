﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.DTOs
{
    public class IngredientDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<CocktailDTO> Cocktails { get; set; } = new List<CocktailDTO>();
        public string ImgPath { get; set; }
    }
}
