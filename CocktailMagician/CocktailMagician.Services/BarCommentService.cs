﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class BarCommentService : IBarCommentService
    {
        private readonly CMDbContext dbContext;
        private readonly IDTOMapper<BarComment, BarCommentDTO> dtoMapper;
        private readonly IFactory factory;

        public BarCommentService(CMDbContext dbContext, IDTOMapper<BarComment, BarCommentDTO> dtoMapper, IFactory factory)
        {
            this.dbContext = dbContext;
            this.dtoMapper = dtoMapper;
            this.factory = factory;
        }
        public async Task<BarCommentDTO> CreateBarCommentAsync(BarCommentDTO newBarComment)
        {
            if(newBarComment == null)
            {
                throw new ArgumentNullException();
            }

            var barComment = this.factory.CreateBarComment(newBarComment.Text);
            barComment.Id = newBarComment.Id;
            barComment.UserId = newBarComment.UserId;
            barComment.BarId = newBarComment.BarId;
            
            await this.dbContext.BarComments.AddAsync(barComment);
            await this.dbContext.SaveChangesAsync();

            return newBarComment;
        }
        public async Task<BarCommentDTO> EditBarCommentAsync(Guid id, string newComment)
        {
            var barComment = await this.dbContext.BarComments
                .FirstOrDefaultAsync(barComment => barComment.Id == id);

            if (barComment == null)
            {
                throw new ArgumentNullException();
            }

            barComment.Text = newComment;
            await this.dbContext.SaveChangesAsync();

            var barCommentDTO = this.dtoMapper.MapToDTO(barComment);

            return barCommentDTO;
        }
        public async Task<bool> DeleteBarCommentAsync(Guid id)
        {
            var barComment = await this.dbContext.BarComments
                .FirstOrDefaultAsync(barComment => barComment.Id == id);

            if (barComment == null)
            {
                return false;
            }

            this.dbContext.BarComments.Remove(barComment);
            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
