﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Constants;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CocktailMagician.Services
{
    public class BarService : IBarService
    {
        private readonly CMDbContext dbContext;
        private readonly IDTOMapper<Bar, BarDTO> dtoMapper;
        private readonly IFactory factory;
        private readonly IDateTimeProvider dateTimeProvider;

        public BarService(CMDbContext dbContext, IDTOMapper<Bar, BarDTO> dtoMapper, IFactory factory, IDateTimeProvider dateTimeProvider)
        {
            this.dbContext = dbContext;
            this.dtoMapper = dtoMapper;
            this.factory = factory;
            this.dateTimeProvider = dateTimeProvider;
        }
        public async Task<BarDTO> GetBarAsync(Guid id)
        {
            var bar = await this.dbContext.Bars
                .Where(bar => bar.IsDeleted == false)
                .Include(bar => bar.Cocktails).ThenInclude(c => c.Cocktail)
                .Include(bar => bar.Comments).ThenInclude(co => co.User)
                .FirstOrDefaultAsync(bar => bar.Id == id);

            if (bar == null)
            {
                throw new ArgumentNullException();
            }

            var barDTO = this.dtoMapper.MapToDTO(bar);

            return barDTO;
        }
        public async Task<BarDTO> CreateBarAsync(BarDTO newBar)
        {
            if (newBar == null)
            {
                throw new ArgumentNullException();
            }

            if (this.dbContext.Bars.Any(b => b.Name == newBar.Name && b.IsDeleted == false))
                throw new ArgumentException();

            var bar = this.factory.CreateBar(newBar.Name, newBar.Description, newBar.Phone, newBar.Address);
            bar.ImgPath = newBar.ImgPath;
            bar.CreatedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.Bars.AddAsync(bar);
            await this.dbContext.SaveChangesAsync();

            return newBar;
        }
        public async Task<BarDTO> EditBarAsync(Guid id, string newName, string newDescription, string newPhone, string newAddress, string imgPath)
        {
            var bar = await this.dbContext.Bars
                .Where(bar => bar.IsDeleted == false)
                .FirstOrDefaultAsync(bar => bar.Id == id);

            if (bar == null)
            {
                throw new ArgumentNullException();
            }
            if (this.dbContext.Bars.Any(b => b.Name == newName && b.IsDeleted == false && b.Id != bar.Id))
                throw new ArgumentException();

            bar.Name = newName;
            bar.Description = newDescription;
            bar.Phone = newPhone;
            bar.Address = newAddress;
            if (imgPath != null)
            {
                bar.ImgPath = imgPath;
            }
            bar.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.SaveChangesAsync();

            var barDTO = this.dtoMapper.MapToDTO(bar);
            return barDTO;
        }
        public async Task<bool> DeleteBarAsync(Guid id)
        {
            var bar = await this.dbContext.Bars
                .Where(bar => bar.IsDeleted == false)
                .Include(b => b.Cocktails)
                .FirstOrDefaultAsync(bar => bar.Id == id);

            if (bar == null)
            {
                return false;
            }

            this.dbContext.BarCocktails.RemoveRange(bar.Cocktails);

            bar.IsDeleted = true;
            bar.DeletedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.SaveChangesAsync();

            return true;
        }
        public async Task<PageTransferInfo<BarDTO>> GetAllBarsAsync(int page)
        {
            IQueryable<Bar> bars = this.dbContext.Bars
                .Where(bar => bar.IsDeleted == false);


            var returnPage = await PaginatedList<Bar>.CreateAsync(bars, page, ConstantHolder.BarsPageSize);

            var barsDTO = this.dtoMapper.MapToDTO(returnPage);

            return barsDTO;
        }
        public async Task<PageTransferInfo<BarDTO>> SortBarsAsync(string sortingOption, int page)
        {


            if (sortingOption == null)
            {
                sortingOption = "name";
            }

            IQueryable<Bar> bars = this.dbContext.Bars.Where(bar => bar.IsDeleted == false)
                .Include(bar => bar.Cocktails).ThenInclude(c => c.Cocktail);

            switch (sortingOption)
            {
                case "name":
                    bars = bars.OrderBy(bar => bar.Name);
                    break;
                case "name_desc":
                    bars = bars.OrderByDescending(bar => bar.Name);
                    break;
                case "rating":
                    bars = bars.OrderBy(bar => bar.AverageRating);
                    break;
                case "rating_desc":
                    bars = bars.OrderByDescending(bar => bar.AverageRating);
                    break;
                case "address":
                    bars = bars.OrderBy(bar => bar.Address);
                    break;
                case "address_desc":
                    bars = bars.OrderByDescending(bar => bar.Address);
                    break;
            }

            var returnPage = await PaginatedList<Bar>.CreateAsync(bars, page, ConstantHolder.BarsPageSize);

            var barDTOs = dtoMapper.MapToDTO(returnPage);

            return barDTOs;
        }
        public async Task<PageTransferInfo<BarDTO>> FilterBarsAsync(string filterOption, int page)
        {
            IQueryable<Bar> bars = this.dbContext.Bars
                               .Where(bar => bar.IsDeleted == false && (bar.Name.Contains(filterOption)
                                       || bar.Address.Contains(filterOption)))
                               .Include(bar => bar.Cocktails).ThenInclude(c => c.Cocktail);




            var returnPage = await PaginatedList<Bar>.CreateAsync(bars, page, ConstantHolder.BarsPageSize);

            var barsDTO = this.dtoMapper.MapToDTO(returnPage);

            return barsDTO;
        }
        public async Task<ICollection<BarDTO>> GetTopThreeBarsAsync()
        {
            var topBars = await this.dbContext.Bars
                            .Where(bar => bar.IsDeleted == false)
                            .Include(bar => bar.Cocktails).ThenInclude(c => c.Cocktail)
                            .OrderByDescending(bar => bar.AverageRating)
                            .Take(3)
                            .ToListAsync();


            var barsDTO = this.dtoMapper.MapToDTO(topBars);

            return barsDTO;
        }
        public async Task<BarDTO> AddAndRemoveCocktailsAsync(BarDTO changedBar)
        {
            var bar = await this.dbContext.Bars.Where(b => b.IsDeleted == false)
                .Include(b => b.Cocktails)
                .FirstOrDefaultAsync(b => b.Id == changedBar.Id) ?? throw new ArgumentNullException();

            this.dbContext.BarCocktails.RemoveRange(bar.Cocktails);
            bar.Cocktails = changedBar.Cocktails
               .Select(i => factory.CreateBarCocktail(bar.Id, i.Id))
               .ToList();
            bar.ModifiedOn = dateTimeProvider.GetDateTime();

            await this.dbContext.BarCocktails.AddRangeAsync(bar.Cocktails);
            await this.dbContext.SaveChangesAsync();

            return changedBar;
        }
        public async Task<BarDTO> GetDeletedBarAsync(Guid id)
        {
            var bar = await this.dbContext.Bars
                 .Where(b => b.IsDeleted == true)
                 .Include(b => b.Cocktails)
                 .ThenInclude(c => c.Cocktail)
                 .FirstOrDefaultAsync(b => b.Id == id) ?? throw new ArgumentNullException();

            var barDTO = dtoMapper.MapToDTO(bar);
            return barDTO;

        }
        public async Task<PageTransferInfo<BarDTO>> GetAllDeletedBarsAsync(int page)
        {

            IQueryable<Bar> bars = this.dbContext.Bars
                .Where(bar => bar.IsDeleted == true);


            var returnPage = await PaginatedList<Bar>.CreateAsync(bars, page, ConstantHolder.BarsPageSize);

            var barsDTO = this.dtoMapper.MapToDTO(returnPage);

            return barsDTO;
        }
        public async Task<bool> RestoreBarAsync(Guid id)
        {
            var bar = await this.dbContext.Bars.Where(b => b.IsDeleted == true)
                .FirstOrDefaultAsync(b => b.Id == id);
            if (bar == null)
                return false;

            bar.IsDeleted = false;
            bar.DeletedOn = null;
            await this.dbContext.SaveChangesAsync();
            return true;

        }
    }
}
