﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.Constants
{
     internal static class ConstantHolder
    {
        internal const string RootDirectory = "wwwroot/images/";

        internal const int BarsPageSize = 6;
        internal const int CocktailsPageSize = 9;
        internal const int IngredientsPageSize = 18;
    }
}
