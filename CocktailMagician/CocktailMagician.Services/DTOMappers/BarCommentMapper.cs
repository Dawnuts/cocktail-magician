﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class BarCommentMapper : IDTOMapper<BarComment, BarCommentDTO>
    {
        public BarCommentDTO MapToDTO(BarComment barComment)
        {
            if (barComment == null)
                throw new ArgumentNullException();
            var barCommentDTO = new BarCommentDTO
            {
                Id = barComment.Id,
                BarId = barComment.BarId,
                Text = barComment.Text,
                User = barComment.User.UserName,
                UserId = barComment.UserId
            };
            return barCommentDTO;
        }

        public ICollection<BarCommentDTO> MapToDTO(ICollection<BarComment> barComments)
        {
            var barCommentDTOs = barComments.Select(bc => MapToDTO(bc)).ToList();
            return barCommentDTOs;
        }

        public PageTransferInfo<BarCommentDTO> MapToDTO(PaginatedList<BarComment> ingredientsPage)
        {
            var dtoPage = new PageTransferInfo<BarCommentDTO>();
            dtoPage.TotalPages = ingredientsPage.TotalPages;
            dtoPage.PageIndex = ingredientsPage.PageIndex;
            dtoPage.HasNextPage = ingredientsPage.HasNextPage;
            dtoPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            dtoPage.Collection = ingredientsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
