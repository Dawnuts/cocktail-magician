﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class IngredientMapper : IDTOMapper<Ingredient, IngredientDTO>
    {

        public IngredientDTO MapToDTO(Ingredient ingredient)
        {
            if (ingredient == null)
                throw new ArgumentNullException();

            var ingredientDTO = new IngredientDTO
            {
                Name = ingredient.Name,
                Id = ingredient.Id,
                ImgPath = ingredient.ImgPath,
                Cocktails = ingredient.Cocktails
                .Select(c => new CocktailDTO { Name = c.Cocktail.Name,
                    Id = c.CocktailId, ImgPath = c.Cocktail.ImgPath })
                .ToList()
            };
            return ingredientDTO;
        }
        public ICollection<IngredientDTO> MapToDTO(ICollection<Ingredient> ingredients)
        {
            var ingredientDTOs = ingredients.Select(i => MapToDTO(i)).ToList();
            return ingredientDTOs;
        }

        public PageTransferInfo<IngredientDTO> MapToDTO(PaginatedList<Ingredient> ingredientsPage)
        {
            var dtoPage = new PageTransferInfo<IngredientDTO>();
            dtoPage.TotalPages = ingredientsPage.TotalPages;
            dtoPage.PageIndex = ingredientsPage.PageIndex;
            dtoPage.HasNextPage = ingredientsPage.HasNextPage;
            dtoPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            dtoPage.Collection = ingredientsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
