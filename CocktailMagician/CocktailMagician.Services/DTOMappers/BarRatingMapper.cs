﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class BarRatingMapper : IDTOMapper<BarRating, BarRatingDTO>
    {
        public BarRatingDTO MapToDTO(BarRating barRating)
        {
            if (barRating == null)
                throw new ArgumentNullException();
            var barRatingDTO = new BarRatingDTO
            {
                Bar = barRating.Bar.Name,
                BarId = barRating.BarId,
                Rating = barRating.Rating,
                User = barRating.User.UserName,
                UserId = barRating.UserId
            };
            return barRatingDTO;
        }

        public ICollection<BarRatingDTO> MapToDTO(ICollection<BarRating> barRatings)
        {
            var barRatingDTOs = barRatings.Select(br => MapToDTO(br)).ToList();
            return barRatingDTOs;
        }
        public PageTransferInfo<BarRatingDTO> MapToDTO(PaginatedList<BarRating> ingredientsPage)
        {
            var dtoPage = new PageTransferInfo<BarRatingDTO>();
            dtoPage.TotalPages = ingredientsPage.TotalPages;
            dtoPage.PageIndex = ingredientsPage.PageIndex;
            dtoPage.HasNextPage = ingredientsPage.HasNextPage;
            dtoPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            dtoPage.Collection = ingredientsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
