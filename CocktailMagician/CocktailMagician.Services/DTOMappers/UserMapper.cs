﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class UserMapper : IDTOMapper<User, UserDTO>
    {
        public UserDTO MapToDTO(User user)
        {
            var userDTO = new UserDTO
            {
                Name = user.UserName,
                Id = user.Id,
                BannedUntil = user.LockoutEnd,
            };
            if (user.LockoutEnabled == false)
                userDTO.IsAdmin = true;
            return userDTO;
        }

        public ICollection<UserDTO> MapToDTO(ICollection<User> users)
        {
            var userDTOs = users.Select(i => MapToDTO(i)).ToList();
            return userDTOs;
        }

        public PageTransferInfo<UserDTO> MapToDTO(PaginatedList<User> usersPage)
        {
            var dtoPage = new PageTransferInfo<UserDTO>();
            dtoPage.TotalPages = usersPage.TotalPages;
            dtoPage.PageIndex = usersPage.PageIndex;
            dtoPage.HasNextPage = usersPage.HasNextPage;
            dtoPage.HasPreviousPage = usersPage.HasPreviousPage;
            dtoPage.Collection = usersPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
