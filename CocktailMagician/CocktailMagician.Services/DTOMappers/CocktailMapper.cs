﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class CocktailMapper : IDTOMapper<Cocktail, CocktailDTO>
    {
        private readonly IDTOMapper<Ingredient, IngredientDTO> ingredientMapper;
        //TODO:Add comment mapping

        public CocktailMapper(IDTOMapper<Ingredient, IngredientDTO> ingredientMapper)
        {
            this.ingredientMapper = ingredientMapper;
        }
        public CocktailDTO MapToDTO(Cocktail cocktail)
        {
            if (cocktail == null)
                throw new ArgumentNullException();
            var cocktailDTO = new CocktailDTO
            {
                Name = cocktail.Name,
                Id = cocktail.Id,
                Description = cocktail.Description,
                AverageRating = cocktail.AverageRating,
                ImgPath = cocktail.ImgPath,
                Ingredients = cocktail.Ingredients
                .Select(i => new IngredientDTO { Name = i.Ingredient.Name, Id = i.IngredientId, ImgPath = i.Ingredient.ImgPath })
                .ToList(),
                Comments = cocktail.Comments.Select(co => new CocktailCommentDTO
                {
                    Id = co.Id,
                    Text = co.Text,
                    CocktailId = cocktail.Id,
                    User = co.User.UserName,
                    UserId = co.UserId
                }).ToList(),
                Bars = cocktail.Bars.Select(b => new BarDTO { Name = b.Bar.Name, Id = b.BarId, ImgPath = b.Bar.ImgPath }).ToList()
            };
            return cocktailDTO;
        }

        public ICollection<CocktailDTO> MapToDTO(ICollection<Cocktail> cocktails)
        {
            var cocktailsDTO = cocktails.Select(c => MapToDTO(c)).ToList();
            return cocktailsDTO;
        }

        public PageTransferInfo<CocktailDTO> MapToDTO(PaginatedList<Cocktail> cocktailsPage)
        {
            var dtoPage = new PageTransferInfo<CocktailDTO>();
            dtoPage.TotalPages = cocktailsPage.TotalPages;
            dtoPage.PageIndex = cocktailsPage.PageIndex;
            dtoPage.HasNextPage = cocktailsPage.HasNextPage;
            dtoPage.HasPreviousPage = cocktailsPage.HasPreviousPage;
            dtoPage.Collection = cocktailsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
