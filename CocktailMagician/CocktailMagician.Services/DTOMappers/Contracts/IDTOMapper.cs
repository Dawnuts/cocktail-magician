﻿using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.DTOMappers.Contracts
{
    public interface IDTOMapper<TEntity, TDTO>
    {
        TDTO MapToDTO(TEntity entity);

        ICollection<TDTO> MapToDTO(ICollection<TEntity> entities);

        PageTransferInfo<TDTO> MapToDTO(PaginatedList<TEntity> entitiesPage);
    }
}
