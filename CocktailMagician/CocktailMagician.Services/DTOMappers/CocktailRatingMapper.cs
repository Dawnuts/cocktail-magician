﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class CocktailRatingMapper : IDTOMapper<CocktailRating, CocktailRatingDTO>
    {
        public CocktailRatingDTO MapToDTO(CocktailRating cocktailRating)
        {
            if (cocktailRating == null)
                throw new ArgumentNullException();

            var cocktailRatingDTO = new CocktailRatingDTO
            {
                Cocktail = cocktailRating.Cocktail.Name,
                CocktailId = cocktailRating.CocktailId,
                Rating = cocktailRating.Rating,
                User = cocktailRating.User.UserName,
                UserId = cocktailRating.UserId
            };
            return cocktailRatingDTO;
        }

        public ICollection<CocktailRatingDTO> MapToDTO(ICollection<CocktailRating> cocktailRatings)
        {
            var cocktailRatingDTOs = cocktailRatings.Select(cr => MapToDTO(cr)).ToList();
            return cocktailRatingDTOs;
        }
        public PageTransferInfo<CocktailRatingDTO> MapToDTO(PaginatedList<CocktailRating> ingredientsPage)
        {
            var dtoPage = new PageTransferInfo<CocktailRatingDTO>();
            dtoPage.TotalPages = ingredientsPage.TotalPages;
            dtoPage.PageIndex = ingredientsPage.PageIndex;
            dtoPage.HasNextPage = ingredientsPage.HasNextPage;
            dtoPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            dtoPage.Collection = ingredientsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
