﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.DTOMappers
{
    public class CocktailCommentMapper : IDTOMapper<CocktailComment, CocktailCommentDTO>
    {
        public CocktailCommentDTO MapToDTO(CocktailComment cocktailComment)
        {
            if (cocktailComment == null)
                throw new ArgumentNullException();

            var cocktailCommentDTO = new CocktailCommentDTO
            {
                Id = cocktailComment.Id,
                CocktailId = cocktailComment.CocktailId,
                Text = cocktailComment.Text,
                User = cocktailComment.User.UserName,
                UserId = cocktailComment.UserId
            };
            return cocktailCommentDTO;
        }

        public ICollection<CocktailCommentDTO> MapToDTO(ICollection<CocktailComment> cocktailComments)
        {
            var cocktailCommentDTOs = cocktailComments.Select(cc => MapToDTO(cc)).ToList();
            return cocktailCommentDTOs;
        }

        public PageTransferInfo<CocktailCommentDTO> MapToDTO(PaginatedList<CocktailComment> ingredientsPage)
        {
            var dtoPage = new PageTransferInfo<CocktailCommentDTO>();
            dtoPage.TotalPages = ingredientsPage.TotalPages;
            dtoPage.PageIndex = ingredientsPage.PageIndex;
            dtoPage.HasNextPage = ingredientsPage.HasNextPage;
            dtoPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            dtoPage.Collection = ingredientsPage.Select(i => MapToDTO(i)).ToList();

            return dtoPage;
        }
    }
}
