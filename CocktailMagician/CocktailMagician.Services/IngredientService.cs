﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Constants;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly CMDbContext dbContext;
        private readonly IDTOMapper<Ingredient, IngredientDTO> dtoMapper;
        private readonly IFactory factory;
        private readonly IDateTimeProvider dateTimeProvider;

        public IngredientService(CMDbContext dbContext, IDTOMapper<Ingredient, IngredientDTO> dtoMapper, IFactory factory, IDateTimeProvider dateTimeProvider)
        {
            this.dbContext = dbContext;
            this.dtoMapper = dtoMapper;
            this.factory = factory;
            this.dateTimeProvider = dateTimeProvider;
        }
        public async Task<PageTransferInfo<IngredientDTO>> GetAllIngredientsAsync(int page, int ingredientsPerPage = ConstantHolder.IngredientsPageSize)
        {
            IQueryable<Ingredient> ingredients = this.dbContext.Ingredients
                .Where(ingredient => ingredient.IsDeleted == false);

            var returnPage = await PaginatedList<Ingredient>.CreateAsync(ingredients, page, ingredientsPerPage);

            var ingredientsDTO = this.dtoMapper.MapToDTO(returnPage);

            return ingredientsDTO;
        }
        public async Task<IngredientDTO> GetIngredientAsync(Guid id)
        {
            var ingredient = await this.dbContext.Ingredients
                .Where(ingredient => ingredient.IsDeleted == false)
                .Include(i => i.Cocktails)
                .ThenInclude(i => i.Cocktail)
                .FirstOrDefaultAsync(ingredient => ingredient.Id == id);

            if (ingredient == null)
            {
                throw new ArgumentNullException();
            }

            var ingredientDTO = this.dtoMapper.MapToDTO(ingredient);

            return ingredientDTO;
        }
        public async Task<IngredientDTO> CreateIngredientAsync(IngredientDTO newIngredient)
        {

            if (await this.dbContext.Ingredients.AnyAsync(i => i.Name == newIngredient.Name && i.IsDeleted == false))
            {
                throw new ArgumentException($"Ingredient {newIngredient.Name} already exists.");
            }

            var ingredient = this.factory.CreateIngredient(newIngredient.Name);
            ingredient.ImgPath = newIngredient.ImgPath ?? "default-ingredient.jpg";
            ingredient.CreatedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.Ingredients.AddAsync(ingredient);
            await this.dbContext.SaveChangesAsync();

            return newIngredient;
        }
        public async Task<IngredientDTO> EditIngredientAsync(IngredientDTO newIngredient)
        {
            var ingredient = await this.dbContext.Ingredients
                .Where(ingredient => ingredient.IsDeleted == false)
                .FirstOrDefaultAsync(ingredient => ingredient.Id == newIngredient.Id);

            if (await this.dbContext.Ingredients.AnyAsync(i => i.Name == newIngredient.Name && i.IsDeleted == false && i.Id != ingredient.Id))
            {
                throw new ArgumentException($"Ingredient {newIngredient.Name} already exists.");
            }
            if (ingredient == null)
            {
                throw new ArgumentNullException();
            }

            ingredient.Name = newIngredient.Name;
            if (newIngredient.ImgPath != null)
                ingredient.ImgPath = newIngredient.ImgPath;
            ingredient.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.SaveChangesAsync();

            var ingredientDTO = this.dtoMapper.MapToDTO(ingredient);

            return ingredientDTO;
        }
        public async Task<bool> DeleteIngredientAsync(Guid id)
        {
            var ingredient = await this.dbContext.Ingredients
                .Where(ingredient => ingredient.IsDeleted == false)
                .Include(ingredient => ingredient.Cocktails)
                .FirstOrDefaultAsync(ingredient => ingredient.Id == id);

            if (ingredient.Cocktails.Any())
            {
                return false;
            }

            ingredient.IsDeleted = true;
            ingredient.DeletedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
