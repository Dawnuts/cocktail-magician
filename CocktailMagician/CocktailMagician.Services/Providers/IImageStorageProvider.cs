﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Providers
{
   public interface IImageStorageProvider
    {
        public Task<string> StoreImage(IFormFile file, string root);
    }
}
