﻿using CocktailMagician.Services.Constants;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Providers
{
    public class ImageStorageProvider : IImageStorageProvider
    {
        internal readonly string[] fileTypes = { "image/png", "image/jpg", "image/jpeg", "image/gif" };
        public async Task<string> StoreImage(IFormFile file, string root)
        {
            if (!fileTypes.Contains(file.ContentType))
                throw new InvalidDataException();


            var uploadFolder = Path.Combine(Directory.GetCurrentDirectory(), $"{ConstantHolder.RootDirectory}{root}");
            var newFileName = $"{Guid.NewGuid()}_{file.FileName}";
            string fullFilePath = Path.Combine(uploadFolder, newFileName);
            string imagePath = $"/images/{root}/{newFileName}";
            using (var stream = new FileStream(fullFilePath, FileMode.Create))
            {
                using (var memoryStream = new MemoryStream())
                {
                    await file.CopyToAsync(memoryStream);
                    var x = memoryStream.ToArray();
                    stream.Write(x, 0, x.Length);
                }
            }
            return imagePath;
        }
    }
}
