﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.Providers
{
    public class PageTransferInfo<T>
    {
        public int PageIndex { get; set; }

        public int TotalPages { get; set; }

        public bool HasPreviousPage { get; set; }

        public bool HasNextPage { get; set; }

        public ICollection<T> Collection { get; set; }
        
    }
}
