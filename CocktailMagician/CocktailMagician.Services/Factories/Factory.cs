﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Services.Factories
{
    public class Factory : IFactory
    {
        public Ingredient CreateIngredient(string name)
        {
            var ingredient = new Ingredient();
            ingredient.Name = name;

            return ingredient;
        }
        public Bar CreateBar(string name, string description, string phone, string address)
        {
            var bar = new Bar();
            bar.Name = name;
            bar.Description = description;
            bar.Phone = phone;
            bar.Address = address;
            bar.AverageRating = 0;

            return bar;
        }
        public Cocktail CreateCocktail(string name, string description)
        {
            var cocktail = new Cocktail();
            cocktail.Name = name;
            cocktail.Description = description;
            cocktail.AverageRating = 0;

            return cocktail;
        }
       
        public CocktailComment CreateCocktailComment(string comment)
        {
            var cocktailComment = new CocktailComment();
            cocktailComment.Text = comment;

            return cocktailComment;
        }
        public CocktailRating CreateCocktailRating(int rating)
        {
            var cocktailRating = new CocktailRating();
            cocktailRating.Rating = rating;

            return cocktailRating;
        }

        public BarComment CreateBarComment(string comment)
        {
            var barComment = new BarComment();
            barComment.Text = comment;

            return barComment;
        }

        public BarRating CreateBarRating(int rating)
        {
            var barRating = new BarRating();
            barRating.Rating = rating;

            return barRating;
        }

        public CocktailIngredient CreateCocktailIngredient(Guid cocktailId, Guid ingredientId)
        {
            var cocktailIngredient = new CocktailIngredient
            {
                CocktailId = cocktailId,
                IngredientId = ingredientId
            };
            return cocktailIngredient;
        }

        public BarCocktail CreateBarCocktail(Guid barId, Guid cocktailId)
        {
            var barCocktail = new BarCocktail
            {
                BarId = barId,
                CocktailId = cocktailId
            };
            return barCocktail;
        }
    }
}
