﻿using CocktailMagician.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Services.Factories
{
    public interface IFactory
    {
        Ingredient CreateIngredient(string name);
        Bar CreateBar(string name, string description, string phone, string address);
        Cocktail CreateCocktail(string name, string description);
        CocktailComment CreateCocktailComment(string comment);
        CocktailRating CreateCocktailRating(int rating);
        BarComment CreateBarComment(string comment);
        BarRating CreateBarRating(int rating);
        CocktailIngredient CreateCocktailIngredient(Guid cocktailId, Guid ingredientId);
        BarCocktail CreateBarCocktail(Guid barId, Guid cocktailId);
    }
}
