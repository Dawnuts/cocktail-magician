﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class CocktailRatingService : ICocktailRatingService
    {
        private readonly CMDbContext dbContext;
        private readonly IFactory factory;
        private readonly IDTOMapper<CocktailRating, CocktailRatingDTO> mapper;

        public CocktailRatingService(CMDbContext dbContext, IFactory factory,
            IDTOMapper<CocktailRating, CocktailRatingDTO> mapper)
        {
            this.dbContext = dbContext;
            this.factory = factory;
            this.mapper = mapper;
        }
        public async Task<CocktailRatingDTO> CreateCocktailRatingAsync(CocktailRatingDTO cocktailRatingDTO)
        {
            if (cocktailRatingDTO == null)
                throw new ArgumentNullException();

            if (this.dbContext.CocktailRatings.Any(cr => cr.CocktailId == cocktailRatingDTO.CocktailId && cr.UserId == cocktailRatingDTO.UserId))
            {
                await UpdateRating(cocktailRatingDTO);
            }
            else
            {
                var cocktailRating = this.factory.CreateCocktailRating(cocktailRatingDTO.Rating);

                cocktailRating.CocktailId = cocktailRatingDTO.CocktailId;
                cocktailRating.UserId = cocktailRatingDTO.UserId;

                await this.dbContext.CocktailRatings.AddAsync(cocktailRating);
            }

            var bar = await UpdateCocktailRatingAsync(cocktailRatingDTO.CocktailId);

            await this.dbContext.SaveChangesAsync();

            return cocktailRatingDTO;

        }

        public async Task<ICollection<CocktailRatingDTO>> GetAllCocktailRatingsAsync(Guid cocktailId)
        {
            var cocktailRatings = await this.dbContext.CocktailRatings
                .Where(cr => cr.CocktailId == cocktailId)
                .Include(cr => cr.Cocktail)
                .Include(cr => cr.User)
                .ToListAsync();

            var cocktailRatingDTOs = this.mapper.MapToDTO(cocktailRatings);

            return cocktailRatingDTOs;
        }

        private async Task<CocktailRating> UpdateRating(CocktailRatingDTO newCocktailRating)
        {
            var cocktailRating = await this.dbContext.CocktailRatings
                 .FirstOrDefaultAsync(br => br.CocktailId == newCocktailRating.CocktailId && br.UserId == newCocktailRating.UserId);
            cocktailRating.Rating = newCocktailRating.Rating;
            return cocktailRating;
        }

        private async Task<Cocktail> UpdateCocktailRatingAsync(Guid id)
        {
           
                var cocktail = await this.dbContext.Cocktails.Where(c => c.IsDeleted == false)
                    .Include(c => c.Ratings)
                    .FirstOrDefaultAsync(c => c.Id == id);

                cocktail.AverageRating = cocktail.Ratings.Average(r => r.Rating);

                return cocktail;
            
        }
    }
}
