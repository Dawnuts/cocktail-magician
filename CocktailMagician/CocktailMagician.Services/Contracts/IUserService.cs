﻿using CocktailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
   public interface IUserService
    {
        public Task<bool> BanUserAsync(Guid id, DateTimeOffset? until);
        public Task<ICollection<UserDTO>> GetAllUsersAsync();
        public Task<ICollection<UserDTO>> GetBannedUsersAsync();
        public Task<bool> UnbanUserAsync(Guid id);
        public Task<UserDTO> GetUserAsync(Guid id);
    }
}
