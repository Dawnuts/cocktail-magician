﻿using CocktailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface ICocktailCommentService
    {
        Task<CocktailCommentDTO> CreateCocktailCommentAsync(CocktailCommentDTO newCocktailComment);
        Task<CocktailCommentDTO> EditCocktailCommentAsync(Guid id, string newComment);
        Task<bool> DeleteCocktailCommentAsync(Guid id);
    }
}
