﻿using CocktailMagician.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface IBarRatingService
    {
        Task<BarRatingDTO> CreateBarRatingAsync(BarRatingDTO newBarRating);
        Task<ICollection<BarRatingDTO>> GetAllBarRatingsAsync(Guid barId);
    }
}
