﻿using CocktailMagician.Services.Constants;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface IIngredientService
    {
        Task<IngredientDTO> GetIngredientAsync(Guid id);
        Task<IngredientDTO> CreateIngredientAsync(IngredientDTO newIngredient);
        Task<IngredientDTO> EditIngredientAsync(IngredientDTO newIngredient);
        Task<bool> DeleteIngredientAsync(Guid id);
        Task<PageTransferInfo<IngredientDTO>> GetAllIngredientsAsync(int page, int ingredientsPerPage = ConstantHolder.IngredientsPageSize);

    }
}
