﻿using CocktailMagician.Services.Constants;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services.Contracts
{
    public interface ICocktailService
    {
        public Task<CocktailDTO> GetCocktailAsync(Guid id);
        public Task<CocktailDTO> CreateCocktailAsync(CocktailDTO newCocktail);
        public Task<CocktailDTO> EditCocktailAsync(CocktailDTO editedCocktail);
        public Task<CocktailDTO> EditIngredientsAsync(CocktailDTO cocktail);
        public Task<bool> DeleteCocktailAsync(Guid id);
        public Task<CocktailDTO> GetDeletedCocktailAsync(Guid id);
        public Task<bool> RestoreCocktailAsync(Guid id);
        public Task<PageTransferInfo<CocktailDTO>> GetAllCocktailsAsync(int page, int pageSize = ConstantHolder.CocktailsPageSize);
        public Task<PageTransferInfo<CocktailDTO>> GetAllDeletedCocktailsAsync(int page);
        public Task<PageTransferInfo<CocktailDTO>> SortCocktailsAsync(string sortingOption, int page);
        public Task<PageTransferInfo<CocktailDTO>> FilterCocktailsAsync(string filterOption, int page);
        public Task<ICollection<CocktailDTO>> GetTopThreeCocktailsAsync();
    }
}
