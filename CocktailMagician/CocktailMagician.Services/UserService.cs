﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Services
{
    public class UserService : IUserService
    {
        private readonly CMDbContext dbContext;
        private readonly IDTOMapper<User, UserDTO> mapper;

        public UserService(CMDbContext dbContext, IDTOMapper<User, UserDTO> mapper )
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }
        public async Task<UserDTO> GetUserAsync(Guid id)
        {
            var user = await this.dbContext.Users.Where(u => u.IsDeleted == false)
                .FirstOrDefaultAsync(u => u.Id == id) ?? throw new ArgumentNullException();

            var userDTO = mapper.MapToDTO(user);

            return userDTO;
        }
        public async Task<bool> BanUserAsync(Guid id, DateTimeOffset? banEndDate)
        {
            var user = await this.dbContext.Users
                .Where(x => x.LockoutEnabled == true && x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
            {
                return false;
            }
            user.LockoutEnd = banEndDate;
            this.dbContext.SaveChanges();
            return true;
        }
        public async Task<bool> UnbanUserAsync(Guid id)
        {
            var user = await this.dbContext.Users
                .Where(x => x.LockoutEnd != null && x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                return false;

            user.LockoutEnd = null;
           await this.dbContext.SaveChangesAsync();
            return true;
        }
        public async Task<ICollection<UserDTO>> GetAllUsersAsync()
        {
            var users = await this.dbContext.Users
                .Where(x => x.LockoutEnd == null && x.IsDeleted == false)
                .ToListAsync();

            var userDTOs = this.mapper.MapToDTO(users);
            return userDTOs;

        }
        public async Task<ICollection<UserDTO>> GetBannedUsersAsync()
        {
            var users = await this.dbContext.Users
               .Where(x => x.LockoutEnd != null)
               .ToListAsync();

            var userDTOs = this.mapper.MapToDTO(users);
            return userDTOs;
        }

    }
}
