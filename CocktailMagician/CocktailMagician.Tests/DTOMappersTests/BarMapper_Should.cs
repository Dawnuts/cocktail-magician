﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class BarMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var sut = new BarMapper();

            var result = sut.MapToDTO(bar);

            Assert.IsInstanceOfType(result, typeof(BarDTO));
        }

        [TestMethod]
        public void CorrectlyMap_From_Bar_ToDTO()
        {          
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var sut = new BarMapper();

            var result = sut.MapToDTO(bar);

            Assert.AreEqual(result.Id, bar.Id);
            Assert.AreEqual(result.Name, bar.Name);
            Assert.AreEqual(result.Description, bar.Description);
            Assert.AreEqual(result.Address, bar.Address);
            Assert.AreEqual(result.Phone, bar.Phone);
            Assert.AreEqual(result.ImgPath, bar.ImgPath);
        }

        [TestMethod]
        public void Return_CorrectInstanceOfCollection()
        {
            var bars = new List<Bar>()
            {
                new Bar
                {
                    Id = Guid.NewGuid(),
                    Name = "Terminal 1",
                    Description = "Very cool",
                    Phone = "111-222-333",
                    Address = "Sofia, Angel Kanchev 1",
                },

                new Bar
                {
                    Id = Guid.NewGuid(),
                    Name = "RockIt",
                    Description = "So nice",
                    Phone = "111-555-333",
                    Address = "Sofia, Petko Karavelov 5",
                }
            };

            var sut = new BarMapper();

            var result = sut.MapToDTO(bars);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarDTO>));
        }
        [TestMethod]
        public void CorrectlyMapFrom_BarCollection_To_BarDTOCollection()
        {
            var bars = new List<Bar>()
            {
                new Bar
                {
                    Id = Guid.NewGuid(),
                    Name = "Terminal 1",
                    Description = "Very cool",
                    Phone = "111-222-333",
                    Address = "Sofia, Angel Kanchev 1",
                },

                new Bar
                {
                    Id = Guid.NewGuid(),
                    Name = "RockIt",
                    Description = "So nice",
                    Phone = "111-555-333",
                    Address = "Sofia, Petko Karavelov 5",
                }
            };

            var sut = new BarMapper();

            var result = sut.MapToDTO(bars);
            var resultList = result.ToList();
            Assert.AreEqual(2, result.Count);
            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(resultList[i].Id, bars[i].Id);
                Assert.AreEqual(resultList[i].Name, bars[i].Name);
                Assert.AreEqual(resultList[i].Description, bars[i].Description);
                Assert.AreEqual(resultList[i].Address, bars[i].Address);
                Assert.AreEqual(resultList[i].Phone, bars[i].Phone);
                Assert.AreEqual(resultList[i].ImgPath, bars[i].ImgPath);
            }
        }
    }
}
