﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class BarCommentMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var comment = new BarComment
            {
                BarId = bar.Id,
                Bar = bar,
                UserId = user.Id,
                User = user,
                Text = "I like it"
            };

            var sut = new BarCommentMapper();

            var result = sut.MapToDTO(comment);

            Assert.IsInstanceOfType(result, typeof(BarCommentDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_BarComment_ToDTO()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var comment = new BarComment
            {
                BarId = bar.Id,
                Bar = bar,
                UserId = user.Id,
                User = user,
                Text = "I like it"
            };

            var sut = new BarCommentMapper();

            var result = sut.MapToDTO(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.BarId, result.BarId);
            Assert.AreEqual(comment.Text, result.Text);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var comments = new List<BarComment>()
            {
                new BarComment
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user.Id,
                    User = user,
                    Text = "I like it"
                },

                new BarComment
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user2.Id,
                    User = user2,
                    Text = "It was nice"
                }
            };

            var sut = new BarCommentMapper();

            var result = sut.MapToDTO(comments);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarCommentDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_BarComment_ToDTO_Collection()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var comments = new List<BarComment>()
            {
                new BarComment
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user.Id,
                    User = user,
                    Text = "I like it"
                },

                new BarComment
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user2.Id,
                    User = user2,
                    Text = "It was nice"
                }
            };

            var sut = new BarCommentMapper();

            var result = sut.MapToDTO(comments);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(comments[i].UserId, resultList[i].UserId);
                Assert.AreEqual(comments[i].BarId, resultList[i].BarId);
                Assert.AreEqual(comments[i].Text, resultList[i].Text);
            }
            Assert.AreEqual(2, resultList.Count);
        }
    }
}
