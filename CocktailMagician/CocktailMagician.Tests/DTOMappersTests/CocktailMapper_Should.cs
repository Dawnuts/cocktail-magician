﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class CocktailMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();

            var sut = new CocktailMapper(mockMapper.Object);

            var result = sut.MapToDTO(cocktail);

            Assert.IsInstanceOfType(result, typeof(CocktailDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_Cocktail_ToDTO()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();

            var sut = new CocktailMapper(mockMapper.Object);

            var result = sut.MapToDTO(cocktail);

            Assert.AreEqual(cocktail.Id, result.Id);
            Assert.AreEqual(cocktail.Name, result.Name);
            Assert.AreEqual(cocktail.Description, result.Description);
            Assert.AreEqual(cocktail.ImgPath, result.ImgPath);
        }

        [TestMethod]
        public void ReturnCorrectInstance_OfCollection()
        {
            var cocktails = new List<Cocktail>()
            {
                new Cocktail
                {
                    Id = Guid.NewGuid(),
                    Name = "Cuba Libre",
                    Description = "Nice cocktail",
                    ImgPath = "testpath"
                },

                new Cocktail
                {
                    Id = Guid.NewGuid(),
                    Name = "Strawberry Daiquiri",
                    Description = "Sweet",
                    ImgPath = "testpath2"
                }
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();

            var sut = new CocktailMapper(mockMapper.Object);

            var result = sut.MapToDTO(cocktails);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_Cocktail_ToDTO_Collection()
        {
            var cocktails = new List<Cocktail>()
            {
                new Cocktail
                {
                    Id = Guid.NewGuid(),
                    Name = "Cuba Libre",
                    Description = "Nice cocktail",
                    ImgPath = "testpath"
                },

                new Cocktail
                {
                    Id = Guid.NewGuid(),
                    Name = "Strawberry Daiquiri",
                    Description = "Sweet",
                    ImgPath = "testpath2"
                }
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();

            var sut = new CocktailMapper(mockMapper.Object);

            var result = sut.MapToDTO(cocktails);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(cocktails[i].Id, resultList[i].Id);
                Assert.AreEqual(cocktails[i].Name, resultList[i].Name);
                Assert.AreEqual(cocktails[i].Description, resultList[i].Description);
                Assert.AreEqual(cocktails[i].ImgPath, resultList[i].ImgPath);
            }

            Assert.AreEqual(2, resultList.Count);
        }
    }
}
