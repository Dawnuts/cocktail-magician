﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class BarRatingMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var rating = new BarRating
            {
                BarId = bar.Id,
                Bar = bar,
                UserId = user.Id,
                User = user,
                Rating = 5
            };

            var sut = new BarRatingMapper();

            var result = sut.MapToDTO(rating);

            Assert.IsInstanceOfType(result, typeof(BarRatingDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_BarRating_ToDTO()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var rating = new BarRating
            {
                BarId = bar.Id,
                Bar = bar,
                UserId = user.Id,
                User = user,
                Rating = 5
            };

            var sut = new BarRatingMapper();

            var result = sut.MapToDTO(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.BarId, result.BarId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var ratings = new List<BarRating>()
            {
                new BarRating
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user.Id,
                    User = user,
                    Rating = 5
                },

                new BarRating
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user2.Id,
                    User = user2,
                    Rating = 4
                }
            };

            var sut = new BarRatingMapper();

            var result = sut.MapToDTO(ratings);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarRatingDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_BarRating_ToDTO_Collection()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var ratings = new List<BarRating>()
            {
                new BarRating
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user.Id,
                    User = user,
                    Rating = 5
                },

                new BarRating
                {
                    BarId = bar.Id,
                    Bar = bar,
                    UserId = user2.Id,
                    User = user2,
                    Rating = 4
                }
            };

            var sut = new BarRatingMapper();

            var result = sut.MapToDTO(ratings);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ratings[i].UserId, resultList[i].UserId);
                Assert.AreEqual(ratings[i].BarId, resultList[i].BarId);
                Assert.AreEqual(ratings[i].Rating, resultList[i].Rating);
            }
            Assert.AreEqual(2, resultList.Count);
        }
    }
}
