﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class CocktailCommentMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var comment = new CocktailComment
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail,
                UserId = user.Id,
                User = user,
                Text = "I like it"
            };

            var sut = new CocktailCommentMapper();

            var result = sut.MapToDTO(comment);

            Assert.IsInstanceOfType(result, typeof(CocktailCommentDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailComment_ToDTO()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var comment = new CocktailComment
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail,
                UserId = user.Id,
                User = user,
                Text = "I like it"
            };

            var sut = new CocktailCommentMapper();

            var result = sut.MapToDTO(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.CocktailId, result.CocktailId);
            Assert.AreEqual(comment.Text, result.Text);
        }

        [TestMethod]
        public void ReturnCorrectInstance_OfCollection()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var comments = new List<CocktailComment>()
            {
                new CocktailComment
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user.Id,
                    User = user,
                    Text = "I like it"
                },

                new CocktailComment
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user2.Id,
                    User = user2,
                    Text = "Nice one"
                }
            };

            var sut = new CocktailCommentMapper();

            var result = sut.MapToDTO(comments);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailCommentDTO>));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailComment_ToDTO_Collection()
        {
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician2@cm.com"
            };

            var comments = new List<CocktailComment>()
            {
                new CocktailComment
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user.Id,
                    User = user,
                    Text = "Nice one"
                },

                new CocktailComment
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail,
                    UserId = user2.Id,
                    User = user2,
                    Text = "I like it"
                }
            };

            var sut = new CocktailCommentMapper();

            var result = sut.MapToDTO(comments);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(comments[i].UserId, resultList[i].UserId);
                Assert.AreEqual(comments[i].CocktailId, resultList[i].CocktailId);
                Assert.AreEqual(comments[i].Text, resultList[i].Text);
            }
            Assert.AreEqual(2, resultList.Count);
        }
    }
}
