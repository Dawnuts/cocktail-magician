﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.DTOMappersTests
{
    [TestClass]
    public class UserMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var sut = new UserMapper();

            var result = sut.MapToDTO(user);

            Assert.IsInstanceOfType(result, typeof(UserDTO));
        }

        [TestMethod]
        public void MapCorrectly_FromUser_ToDTO()
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "magician@cm.com"
            };

            var sut = new UserMapper();

            var result = sut.MapToDTO(user);

            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.UserName, result.Name);
        }

        [TestMethod]
        public void Return_CorrectInstanceOfCollection()
        {
            var users = new List<User>()
            {
                new User
                {
                    Id = Guid.NewGuid(),
                    UserName = "magician@cm.com"
                },
                new User
                {
                    Id = Guid.NewGuid(),
                    UserName = "magician2@cm.com"
                }
            };

            var sut = new UserMapper();

            var result = sut.MapToDTO(users);

            Assert.IsInstanceOfType(result, typeof(ICollection<UserDTO>));
        }

        [TestMethod]
        public void MapCorrectly_FromUser_ToDTO_Collection()
        {
            var users = new List<User>()
            {
                new User
                {
                    Id = Guid.NewGuid(),
                    UserName = "magician@cm.com"
                },
                new User
                {
                    Id = Guid.NewGuid(),
                    UserName = "magician2@cm.com"
                }
            };

            var sut = new UserMapper();

            var result = sut.MapToDTO(users);
            var resultList = result.ToList();
            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(users[i].Id, resultList[i].Id);
                Assert.AreEqual(users[i].UserName, resultList[i].Name);
            }

            Assert.AreEqual(2, resultList.Count);
        }
    }
 }
