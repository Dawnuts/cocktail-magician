﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class EditBarAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBar_When_ValidParams()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBar_When_ValidParams));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }
            var id = bar.Id;
            var newName = "Terminal 2";
            var newDescription = "Not so cool";
            var newPhone = "111-222-444";
            var newAddress = "Sofia, Angel Kanchev 2";

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.EditBarAsync(id, newName, newDescription, newPhone, newAddress, null);

                var editedBar = await assertContext.Bars.FirstAsync();

                Assert.AreEqual(newName, editedBar.Name);
                Assert.AreEqual(newDescription, editedBar.Description);
                Assert.AreEqual(newPhone, editedBar.Phone);
                Assert.AreEqual(newAddress, editedBar.Address);
            }
        }

        [TestMethod]
        public async Task Throw_When_BarNotFound()
        {
            var options = Utilities.GetOptions(nameof(Throw_When_BarNotFound));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var id = bar.Id;
            var newName = "Terminal 2";
            var newDescription = "Not so cool";
            var newPhone = "111-222-444";
            var newAddress = "Sofia, Angel Kanchev 2";

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditBarAsync(id, newName, newDescription, newPhone, newAddress, null));
            }
        }
    }
}
