﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
   public class GetDeletedBarAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectDeletedBar()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectDeletedBar));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                IsDeleted = true
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>()))
                .Returns<Bar>(b => new BarDTO
                {
                    Name = b.Name,
                    Id = b.Id,
                    Description = b.Description,
                    Phone = b.Phone,
                    Address = b.Address,
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }

            var id = bar.Id;

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.GetDeletedBarAsync(id);
                var dbBar = await assertContext.Bars.Where(b => b.IsDeleted)
                    .FirstOrDefaultAsync(b => b.Id == id);

                Assert.AreEqual(dbBar.Name, result.Name);
                Assert.AreEqual(dbBar.Description, result.Description);
                Assert.AreEqual(dbBar.Phone, result.Phone);
                Assert.AreEqual(dbBar.Address, result.Address);
            }
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenNoDeletedBar()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsExceptionWhenNoDeletedBar));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var id = bar.Id;

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetDeletedBarAsync(id));
            }
        }
        
    }
}
