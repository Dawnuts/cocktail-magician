﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class GetAllDeletedBarsAsync_Should
    {
        [TestMethod]
        public async Task ReturnsAllDeletedBars()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsAllDeletedBars));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                IsDeleted = true
            };

            var bar2 = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt",
                Description = "So nice",
                Phone = "111-555-333",
                Address = "Sofia, Petko Karavelov 5",
                IsDeleted = true
            };

            var barList = new List<Bar> { bar, bar2 };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Bar>>()))
                .Returns<PaginatedList<Bar>>(pt =>
                new PageTransferInfo<BarDTO>
                {
                    HasNextPage = pt.HasNextPage,
                    HasPreviousPage = pt.HasPreviousPage,
                    TotalPages = pt.TotalPages,
                    PageIndex = pt.PageIndex,
                    Collection = barList
                    .Select(b => new BarDTO { Address = b.Address, Name = b.Name, Id = b.Id, Phone = b.Phone,
                    Description = b.Description}).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddRangeAsync(barList);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using(var assertContext =new CMDbContext(options))
            {
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);
                var result = sut.GetAllDeletedBarsAsync(1);

                var dbBars = await assertContext.Bars.Select(b => b.IsDeleted).ToListAsync();
            }

        }

    }
}
