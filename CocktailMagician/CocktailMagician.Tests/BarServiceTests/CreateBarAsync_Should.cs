﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class CreateBarAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBar()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBar));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var barDTO = new BarDTO
            {
                Id = bar.Id,
                Name = bar.Name,
                Description = bar.Description,
                Phone = bar.Phone,
                Address = bar.Address
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateBar(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                       .Returns(bar);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Bar>())).Returns(barDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.CreateBarAsync(barDTO);

                Assert.IsInstanceOfType(result, typeof(BarDTO));

                Assert.AreEqual("Terminal 1", result.Name);
                Assert.AreEqual("Very cool", result.Description);
                Assert.AreEqual("111-222-333", result.Phone);
                Assert.AreEqual("Sofia, Angel Kanchev 1", result.Address);
            }
        }
    }
}
