﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarServiceTests
{
    [TestClass]
    public class RestoreBarAsync_Should
    {
        [TestMethod]
        public async Task ReturnsTrueAndRestoresBar()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsTrueAndRestoresBar));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                IsDeleted = true
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }

            var id = bar.Id;

            //Act&Assert
            using(var assertContext = new CMDbContext(options))
            {
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.RestoreBarAsync(id);

                var dbBar = await assertContext.Bars.FirstOrDefaultAsync(b => b.Id == id);

                Assert.IsTrue(result);

                Assert.AreEqual(id, dbBar.Id);
                Assert.AreEqual(bar.Name, dbBar.Name);
                Assert.AreEqual(bar.Description, dbBar.Description);
                Assert.AreEqual(bar.Address, dbBar.Address);
                Assert.AreEqual(bar.Phone, dbBar.Phone);
                Assert.IsFalse(dbBar.IsDeleted);
            }
        }
        [TestMethod]
        public async Task ReturnsFalseWhenNoBarToRestore()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsFalseWhenNoBarToRestore));

            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };

            var mockMapper = new Mock<IDTOMapper<Bar, BarDTO>>();
            var mockFactory = new Mock<IFactory>();
            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.SaveChangesAsync();
            }

            var id = bar.Id;

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new BarService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);
                var result = await sut.RestoreBarAsync(id);

                Assert.IsFalse(result);
            }
        }
    }
}
