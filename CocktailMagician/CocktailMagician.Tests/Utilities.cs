using CocktailMagician.Data;
using Microsoft.EntityFrameworkCore;
using System;

namespace CocktailMagician.Tests
{
    public class Utilities
    {
        public static DbContextOptions<CMDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<CMDbContext>()
                .UseInMemoryDatabase(databaseName).Options;
        }
    }
}
