﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class UserViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToViewModel(user);

            Assert.IsInstanceOfType(result, typeof(UserViewModel));
        }

        [TestMethod]
        public void MapCorrectly_FromUserDTO_ToViewModel()
        {
            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToViewModel(user);

            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.Name, result.Name);
        }

        [TestMethod]
        public void Return_CorrectInstanceOfCollection()
        {
            var users = new List<UserDTO>()
            {
                new UserDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "magician@cm.com"
                },
                new UserDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "magician2@cm.com"
                }
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToViewModel(users);

            Assert.IsInstanceOfType(result, typeof(ICollection<UserViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_FromUserDTO_ToViewModel_Collection()
        {
            var users = new List<UserDTO>()
            {
                new UserDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "magician@cm.com"
                },
                new UserDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "magician2@cm.com"
                }
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToViewModel(users);
            var resultList = result.ToList();
            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(users[i].Id, resultList[i].Id);
                Assert.AreEqual(users[i].Name, resultList[i].Name);
            }

            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToDTO(user);

            Assert.IsInstanceOfType(result, typeof(UserDTO));
        }

        [TestMethod]
        public void MapCorrectly_FromUserViewModel_ToDTO()
        {
            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var sut = new UserViewModelMapper();

            var result = sut.MapToDTO(user);

            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.Name, result.Name);
        }
    }
}
