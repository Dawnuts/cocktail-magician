﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class CocktailRatingViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new CocktailRatingDTO
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail.Name,
                UserId = user.Id,
                User = user.Name,
                Rating = 5
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToViewModel(rating);

            Assert.IsInstanceOfType(result, typeof(CocktailRatingViewModel));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailRatingDTO_ToViewModel()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new CocktailRatingDTO
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail.Name,
                UserId = user.Id,
                User = user.Name,
                Rating = 5
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToViewModel(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.CocktailId, result.CocktailId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }

        [TestMethod]
        public void ReturnCorrectInstance_OfCollection()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var ratings = new List<CocktailRatingDTO>()
            {
                new CocktailRatingDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Rating = 5
                },

                new CocktailRatingDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Rating = 4
                }
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToViewModel(ratings);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailRatingViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailRatingDTO_ToViewModel_Collection()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var ratings = new List<CocktailRatingDTO>()
            {
                new CocktailRatingDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Rating = 5
                },

                new CocktailRatingDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Rating = 4
                }
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToViewModel(ratings);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ratings[i].UserId, resultList[i].UserId);
                Assert.AreEqual(ratings[i].CocktailId, resultList[i].CocktailId);
                Assert.AreEqual(ratings[i].Rating, resultList[i].Rating);
            }
            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new CocktailRatingViewModel
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Rating = 5
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToDTO(rating);

            Assert.IsInstanceOfType(result, typeof(CocktailRatingDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailRatingViewModel_ToDTO()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new CocktailRatingViewModel
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Rating = 5
            };

            var sut = new CocktailRatingViewModelMapper();

            var result = sut.MapToDTO(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.CocktailId, result.CocktailId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }
    }
}
