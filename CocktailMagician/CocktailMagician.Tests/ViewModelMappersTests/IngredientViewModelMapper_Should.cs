﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class IngredientViewModelMapper_Should
    {
        [TestMethod]
        public void Return_CorrectInstance()
        {
            var ingredient = new IngredientDTO
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToViewModel(ingredient);

            Assert.IsInstanceOfType(result, typeof(IngredientViewModel));
        }

        [TestMethod]
        public void MapCorrectly_FromIngredientDTO_ToViewModel()
        {
            var ingredient = new IngredientDTO
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToViewModel(ingredient);

            Assert.AreEqual(ingredient.Id, result.Id);
            Assert.AreEqual(ingredient.Name, result.Name);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var ingredients = new List<IngredientDTO>()
            {
                new IngredientDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Wine"
                },

                new IngredientDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Blueberry",
                }
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToViewModel(ingredients);

            Assert.IsInstanceOfType(result, typeof(ICollection<IngredientViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_IngredientDTO_ToViewModel_Collection()
        {
            var ingredients = new List<IngredientDTO>()
            {
                new IngredientDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Wine"
                },

                new IngredientDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Blueberry",
                }
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToViewModel(ingredients);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ingredients[i].Id, resultList[i].Id);
                Assert.AreEqual(ingredients[i].Name, resultList[i].Name);
            }

            Assert.AreEqual(2, resultList.Count());
        }

        [TestMethod]
        public void ReturnCorrectDTOInstance()
        {
            var ingredient = new IngredientViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToDTO(ingredient);

            Assert.IsInstanceOfType(result, typeof(IngredientDTO));
        }

        [TestMethod]
        public void MapCorrectly_FromIngredientViewModel_ToDTO()
        {
            var ingredient = new IngredientViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var sut = new IngredientViewModelMapper();

            var result = sut.MapToDTO(ingredient);

            Assert.AreEqual(ingredient.Id, result.Id);
            Assert.AreEqual(ingredient.Name, result.Name);
        }
    }
}
