﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class CocktailCommentViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new CocktailCommentDTO
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail.Name,
                UserId = user.Id,
                User = user.Name,
                Text = "I like it"
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToViewModel(comment);

            Assert.IsInstanceOfType(result, typeof(CocktailCommentViewModel));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailCommentDTO_ToViewModel()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new CocktailCommentDTO
            {
                CocktailId = cocktail.Id,
                Cocktail = cocktail.Name,
                UserId = user.Id,
                User = user.Name,
                Text = "I like it"
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToViewModel(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.CocktailId, result.CocktailId);
            Assert.AreEqual(comment.Text, result.Text);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var comments = new List<CocktailCommentDTO>()
            {
                new CocktailCommentDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Text = "I like it"
                },

                new CocktailCommentDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Text = "It was nice"
                }
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToViewModel(comments);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailCommentViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailCommentDTO_ToViewModel_Collection()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var comments = new List<CocktailCommentDTO>()
            {
                new CocktailCommentDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Text = "I like it"
                },

                new CocktailCommentDTO
                {
                    CocktailId = cocktail.Id,
                    Cocktail = cocktail.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Text = "It was nice"
                }
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToViewModel(comments);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(comments[i].UserId, resultList[i].UserId);
                Assert.AreEqual(comments[i].CocktailId, resultList[i].CocktailId);
                Assert.AreEqual(comments[i].Text, resultList[i].Text);
            }
            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new CocktailCommentViewModel
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Text = "I like it"
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToDTO(comment);

            Assert.IsInstanceOfType(result, typeof(CocktailCommentDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailCommentViewModel_ToDTO()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var comment = new CocktailCommentViewModel
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Text = "I like it"
            };

            var sut = new CocktailCommentViewModelMapper();

            var result = sut.MapToDTO(comment);

            Assert.AreEqual(comment.UserId, result.UserId);
            Assert.AreEqual(comment.CocktailId, result.CocktailId);
            Assert.AreEqual(comment.Text, result.Text);
        }
    }
}
