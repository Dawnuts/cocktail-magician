﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class CocktailViewModelMapper_Should
    {
        [TestMethod]
        public void Return_CorrectInstance()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath",
                Ingredients = new List<IngredientDTO>
                {
                    new IngredientDTO
                    {
                        Id = Guid.NewGuid(),
                        Name = "Rum",
                        ImgPath = "test"
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToViewModel(cocktail);

            Assert.IsInstanceOfType(result, typeof(CocktailViewModel));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailDTO_ToViewModel()
        {
            var cocktail = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath",
                Ingredients = new List<IngredientDTO>
                {
                    new IngredientDTO
                    {
                        Id = Guid.NewGuid(),
                        Name = "Rum",
                        ImgPath = "test"
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToViewModel(cocktail);

            Assert.AreEqual(cocktail.Id, result.Id);
            Assert.AreEqual(cocktail.Name, result.Name);
            Assert.AreEqual(cocktail.Description, result.Description);
            Assert.AreEqual(cocktail.ImgPath, result.ImgPath);
        }

        [TestMethod]
        public void ReturnCorrectInstance_OfCollection()
        {
            var cocktails = new List<CocktailDTO>()
            {
                new CocktailDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Cuba Libre",
                    Description = "Nice cocktail",
                    ImgPath = "testpath",
                    Ingredients = new List<IngredientDTO>
                    {
                        new IngredientDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Rum",
                            ImgPath = "test"
                        }
                    }
                },

                new CocktailDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Strawberry Daiquiri",
                    Description = "Sweet",
                    ImgPath = "testpath2",
                    Ingredients = new List<IngredientDTO>
                    {
                        new IngredientDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Strawberry",
                            ImgPath = "test2"
                        }
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToViewModel(cocktails);

            Assert.IsInstanceOfType(result, typeof(ICollection<CocktailViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailDTO_ToViewModel_Collection()
        {
            var cocktails = new List<CocktailDTO>()
            {
                new CocktailDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Cuba Libre",
                    Description = "Nice cocktail",
                    ImgPath = "testpath",
                    Ingredients = new List<IngredientDTO>
                    {
                        new IngredientDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Rum",
                            ImgPath = "test"
                        }
                    }
                },

                new CocktailDTO
                {
                    Id = Guid.NewGuid(),
                    Name = "Strawberry Daiquiri",
                    Description = "Sweet",
                    ImgPath = "testpath2",
                    Ingredients = new List<IngredientDTO>
                    {
                        new IngredientDTO
                        {
                            Id = Guid.NewGuid(),
                            Name = "Strawberry",
                            ImgPath = "test2"
                        }
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToViewModel(cocktails);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(cocktails[i].Id, resultList[i].Id);
                Assert.AreEqual(cocktails[i].Name, resultList[i].Name);
                Assert.AreEqual(cocktails[i].Description, resultList[i].Description);
                Assert.AreEqual(cocktails[i].ImgPath, resultList[i].ImgPath);
            }

            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath",
                Ingredients = new List<IngredientViewModel>
                {
                    new IngredientViewModel
                    {
                        Id = Guid.NewGuid(),
                        Name = "Rum",
                        ImgPath = "test"
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToDTO(cocktail);

            Assert.IsInstanceOfType(result, typeof(CocktailDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_CocktailViewModel_ToDTO()
        {
            var cocktail = new CocktailViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre",
                Description = "Nice cocktail",
                ImgPath = "testpath",
                Ingredients = new List<IngredientViewModel>
                {
                    new IngredientViewModel
                    {
                        Id = Guid.NewGuid(),
                        Name = "Rum",
                        ImgPath = "test"
                    }
                }
            };

            var sut = new CocktailViewModelMapper();

            var result = sut.MapToDTO(cocktail);

            Assert.AreEqual(cocktail.Id, result.Id);
            Assert.AreEqual(cocktail.Name, result.Name);
            Assert.AreEqual(cocktail.Description, result.Description);
            Assert.AreEqual(cocktail.ImgPath, result.ImgPath);
        }
    }
}
