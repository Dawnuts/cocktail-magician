﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailMagician.Tests.ViewModelMappersTests
{
    [TestClass]
    public class BarRatingViewModelMapper_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new BarRatingDTO
            {
                BarId = bar.Id,
                Bar = bar.Name,
                UserId = user.Id,
                User = user.Name,
                Rating = 5
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToViewModel(rating);

            Assert.IsInstanceOfType(result, typeof(BarRatingViewModel));
        }

        [TestMethod]
        public void MapCorrectly_From_BarRatingDTO_ToViewModel()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new BarRatingDTO
            {
                BarId = bar.Id,
                Bar = bar.Name,
                UserId = user.Id,
                User = user.Name,
                Rating = 5
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToViewModel(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.BarId, result.BarId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }

        [TestMethod]
        public void ReturnCorrectInstance_Of_Collection()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var ratings = new List<BarRatingDTO>()
            {
                new BarRatingDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Rating = 5
                },

                new BarRatingDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Rating = 4
                }
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToViewModel(ratings);

            Assert.IsInstanceOfType(result, typeof(ICollection<BarRatingViewModel>));
        }

        [TestMethod]
        public void MapCorrectly_From_BarRatingDTO_ToViewModel_Collection()
        {
            var bar = new BarDTO
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var user2 = new UserDTO
            {
                Id = Guid.NewGuid(),
                Name = "magician2@cm.com"
            };

            var ratings = new List<BarRatingDTO>()
            {
                new BarRatingDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user.Id,
                    User = user.Name,
                    Rating = 5
                },

                new BarRatingDTO
                {
                    BarId = bar.Id,
                    Bar = bar.Name,
                    UserId = user2.Id,
                    User = user2.Name,
                    Rating = 4
                }
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToViewModel(ratings);
            var resultList = result.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(ratings[i].UserId, resultList[i].UserId);
                Assert.AreEqual(ratings[i].BarId, resultList[i].BarId);
                Assert.AreEqual(ratings[i].Rating, resultList[i].Rating);
            }
            Assert.AreEqual(2, resultList.Count);
        }

        [TestMethod]
        public void Return_CorrectInstance()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new BarRatingViewModel
            {
                BarId = bar.Id,
                Bar = bar.Name,
                UserId = user.Id,
                User = user.Name,
                Rating = 5
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToDTO(rating);

            Assert.IsInstanceOfType(result, typeof(BarRatingDTO));
        }

        [TestMethod]
        public void MapCorrectly_From_BarRatingViewModel_ToDTO()
        {
            var bar = new BarViewModel
            {
                Id = Guid.NewGuid(),
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
                ImgPath = "testpath"
            };

            var user = new UserViewModel
            {
                Id = Guid.NewGuid(),
                Name = "magician@cm.com"
            };

            var rating = new BarRatingViewModel
            {
                BarId = bar.Id,
                UserId = user.Id,
                Rating = 5
            };

            var sut = new BarRatingViewModelMapper();

            var result = sut.MapToDTO(rating);

            Assert.AreEqual(rating.UserId, result.UserId);
            Assert.AreEqual(rating.BarId, result.BarId);
            Assert.AreEqual(rating.Rating, result.Rating);
        }
    }
}
