﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateBarCocktail_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt"
            };

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre"
            };

            var sut = new Factory();

            var result = sut.CreateBarCocktail(bar.Id, cocktail.Id);

            Assert.IsInstanceOfType(result, typeof(BarCocktail));
        }

        [TestMethod]
        public void ReturnCorrectBarCocktail()
        {
            var bar = new Bar
            {
                Id = Guid.NewGuid(),
                Name = "RockIt"
            };

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "Cuba Libre"
            };

            var sut = new Factory();

            var result = sut.CreateBarCocktail(bar.Id, cocktail.Id);

            Assert.AreEqual(bar.Id, result.BarId);
            Assert.AreEqual(cocktail.Id, result.CocktailId);
        }
    }
}
