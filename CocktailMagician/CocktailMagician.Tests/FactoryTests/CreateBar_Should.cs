﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateBar_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var name = "RockIt";
            var description = "Very cool";
            var phone = "111-222-333";
            var address = "Sofia, Angel Kanchev 1";
            
            var sut = new Factory();

            var result = sut.CreateBar(name, description, phone, address);

            Assert.IsInstanceOfType(result, typeof(Bar));
        }

        [TestMethod]
        public void ReturnCorrectBar()
        {
            var name = "RockIt";
            var description = "Very cool";
            var phone = "111-222-333";
            var address = "Sofia, Angel Kanchev 1";
            
            var sut = new Factory();

            var result = sut.CreateBar(name, description, phone, address);

            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(description, result.Description);
            Assert.AreEqual(phone, result.Phone);
            Assert.AreEqual(address, result.Address);
        }
    }
}
