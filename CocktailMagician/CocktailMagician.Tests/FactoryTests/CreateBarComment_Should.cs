﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateBarComment_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var comment = "Very cool";

            var sut = new Factory();

            var result = sut.CreateBarComment(comment);

            Assert.IsInstanceOfType(result, typeof(BarComment));
        }

        [TestMethod]
        public void ReturnCorrectComment()
        {
            var comment = "Very cool";

            var sut = new Factory();

            var result = sut.CreateBarComment(comment);

            Assert.AreEqual(comment, result.Text);
        }
    }
}
