﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailMagician.Tests.FactoryTests
{
    [TestClass]
    public class CreateBarRating_Should
    {
        [TestMethod]
        public void ReturnCorrectInstance()
        {
            var rating = 4;

            var sut = new Factory();

            var result = sut.CreateBarRating(rating);

            Assert.IsInstanceOfType(result, typeof(BarRating));
        }

        [TestMethod]
        public void ReturnCorrectRating()
        {
            var rating = 4;

            var sut = new Factory();

            var result = sut.CreateBarRating(rating);

            Assert.AreEqual(rating, result.Rating);
        }
    }
}
