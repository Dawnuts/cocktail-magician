﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailCommentServiceTests
{
    [TestClass]
    public class DeleteCocktailComment_Should
    {
        [TestMethod]
        public async Task ReturnsTrue_And_DeletesCocktailComment()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsTrue_And_DeletesCocktailComment));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            var cocktailComment = new CocktailComment
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = commentId
            };

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.CocktailComments.AddAsync(cocktailComment);
                await arrangeContext.SaveChangesAsync();
            }


            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.DeleteCocktailCommentAsync(commentId);

                Assert.IsTrue(result);
                Assert.IsFalse(assertContext.CocktailComments.Any(cc => cc.Id == commentId));
            }
        }
        [TestMethod]
        public async Task ReturnFalse_When_NoSuchCocktailComment()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_NoSuchCocktailComment));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.DeleteCocktailCommentAsync(commentId);

                Assert.IsFalse(result);
            }
        }
    }
}
