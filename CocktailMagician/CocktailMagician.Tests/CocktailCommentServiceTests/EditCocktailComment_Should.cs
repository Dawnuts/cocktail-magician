﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailCommentServiceTests
{
    [TestClass]
    public class EditCocktailComment_Should
    {
        [TestMethod]
        public async Task ReturnEditedCocktailComment()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnEditedCocktailComment));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            var cocktailComment = new CocktailComment
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = commentId
            };

            string newComment = "EditedTestComment";
            var editedCocktailCommentDTO = new CocktailCommentDTO
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = newComment,
                Id = commentId
            };

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.CocktailComments.AddAsync(cocktailComment);
                await arrangeContext.SaveChangesAsync();
            }

            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailComment>()))
              .Returns(editedCocktailCommentDTO);

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.EditCocktailCommentAsync(commentId, newComment);

                var dbEditedCocktailComment = await assertContext.CocktailComments.FindAsync(commentId);

                Assert.AreEqual(result.Id, dbEditedCocktailComment.Id);
                Assert.AreEqual(newComment, dbEditedCocktailComment.Text);
            }

        }
        [TestMethod]
        public async Task ThrowsNullException_When_NoCommentToEdit()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsNullException_When_NoCommentToEdit));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            var cocktailComment = new CocktailComment
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = Guid.NewGuid()
            };

            string newComment = "EditedTestComment";
            var editedCocktailCommentDTO = new CocktailCommentDTO
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = newComment,
                Id = commentId
            };

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.CocktailComments.AddAsync(cocktailComment);
                await arrangeContext.SaveChangesAsync();
            }

            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailComment>()))
              .Returns(editedCocktailCommentDTO);

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditCocktailCommentAsync(commentId, newComment));
            }
        }
    }
}
