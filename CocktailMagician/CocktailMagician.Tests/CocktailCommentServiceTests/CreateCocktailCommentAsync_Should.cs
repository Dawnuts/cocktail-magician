﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailCommentServiceTests
{
    [TestClass]
    public class CreateCocktailCommentAsync_Should
    {
        [TestMethod]

        public async Task ReturnNewCocktailComment()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnNewCocktailComment));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            var cocktailComment = new CocktailComment
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = commentId
            };

            var cocktailCommentDTO = new CocktailCommentDTO
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = commentId
            };

            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(x => x.CreateCocktailComment(It.IsAny<string>()))
                .Returns(new CocktailComment { Text = cocktailComment.Text, Id = commentId });

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailComment>()))
                .Returns(cocktailCommentDTO);

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.CreateCocktailCommentAsync(cocktailCommentDTO);
                var dbCocktailComment = await assertContext.CocktailComments.FindAsync(commentId);

                Assert.AreEqual(dbCocktailComment.Id, result.Id);
                Assert.AreEqual(dbCocktailComment.CocktailId, result.CocktailId);
                Assert.AreEqual(dbCocktailComment.UserId, result.UserId);
                Assert.AreEqual(dbCocktailComment.Text, result.Text);
            }

        }
        [TestMethod]

        public async Task ThrowsNullException_WhenCocktailCommentIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsNullException_WhenCocktailCommentIsNull));

            var cocktailId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var commentId = Guid.NewGuid();

            var cocktail = new Cocktail
            {
                Id = cocktailId,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = userId,
                UserName = "TestUser",
            };

            var cocktailComment = new CocktailComment
            {
                UserId = user.Id,
                CocktailId = cocktail.Id,
                Text = "TestComment",
                Id = commentId
            };
            CocktailCommentDTO cocktailCommentDTO = null;

            var mockMapper = new Mock<IDTOMapper<CocktailComment, CocktailCommentDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(x => x.CreateCocktailComment(It.IsAny<string>()))
                .Returns(new CocktailComment { Text = cocktailComment.Text, Id = commentId });

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailComment>()))
                .Returns(cocktailCommentDTO);

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailCommentService(assertContext, mockFactory.Object, mockMapper.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateCocktailCommentAsync(cocktailCommentDTO));
            }
        }
    }
}
