﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailRatingServiceTests
{
    [TestClass]
   public class CreateCocktailRatingAsync_Should
    {
        [TestMethod]

        public async Task ReturnCreatedCocktailRating()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCreatedCocktailRating));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "TestUser",

            };

            var cocktailRatingDTO = new CocktailRatingDTO
            {
                CocktailId = cocktail.Id,
                UserId = user.Id,
                Rating = 4
            };

            var cocktailRating = new CocktailRating { Rating = 4 };


            var mockMapper = new Mock<IDTOMapper<CocktailRating, CocktailRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailRating>())).Returns(cocktailRatingDTO);
            mockFactory.Setup(x => x.CreateCocktailRating(It.IsAny<int>())).Returns(cocktailRating);


            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using(var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailRatingService(assertContext, mockFactory.Object, mockMapper.Object);

                var result = await sut.CreateCocktailRatingAsync(cocktailRatingDTO);

                var dbCocktailRating = await assertContext.CocktailRatings
                    .Where(cr => cr.CocktailId == cocktail.Id && cr.UserId == user.Id)
                    .FirstOrDefaultAsync();

                Assert.AreEqual(4, dbCocktailRating.Rating);
            }
        }
        [TestMethod]

        public async Task ThrowsException_When_CocktailRatingIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsException_When_CocktailRatingIsNull));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = "TestUser",

            };

            CocktailRatingDTO cocktailRatingDTO = null;

            var mockMapper = new Mock<IDTOMapper<CocktailRating, CocktailRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<CocktailRating>())).Returns(cocktailRatingDTO);


            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailRatingService(assertContext, mockFactory.Object, mockMapper.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateCocktailRatingAsync(cocktailRatingDTO));
            }
        }
    }
}
