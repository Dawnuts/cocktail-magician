﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.IngredientServiceTests
{
    [TestClass]
    public class GetAllIngredientsAsync_Should
    {
        [TestMethod]
        public async Task Return_CorrectIngredients()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectIngredients));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine",
            };

            var ingredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Blueberry",
            };

            var ingredientsList = new List<Ingredient> { ingredient, ingredient2 };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            var ingredients = new List<IngredientDTO>()
            { 
                new IngredientDTO 
                { 
                    Id = ingredient.Id, 
                    Name = ingredient.Name 
                }, 
                new IngredientDTO 
                { 
                    Id = ingredient2.Id, 
                    Name = ingredient2.Name 
                } 
            };

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Ingredient>>()))
                .Returns<PaginatedList<Ingredient>>(pl => new PageTransferInfo<IngredientDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = ingredients
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.Ingredients.AddAsync(ingredient2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.GetAllIngredientsAsync(1);

                var resultList = result.Collection.ToList();

                for (int i = 0; i < resultList.Count; i++)
                {
                    Assert.AreEqual(ingredientsList[i].Id, resultList[i].Id);
                    Assert.AreEqual(ingredientsList[i].Name, resultList[i].Name);
                }
            }

        }

      //  [TestMethod]
      //  public async Task ThrowWhen_IngredientsNotFound()
      //  {
      //      var options = Utilities.GetOptions(nameof(ThrowWhen_IngredientsNotFound));
      //
      //      var ingredient = new Ingredient
      //      {
      //          Id = Guid.NewGuid(),
      //          Name = "Wine"
      //      };
      //
      //      var ingredient2 = new Ingredient
      //      {
      //          Id = Guid.NewGuid(),
      //          Name = "Blueberry",
      //      };
      //
      //      var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
      //      var mockFactory = new Mock<IFactory>();
      //
      //      var ingredients = new List<IngredientDTO>()
      //      {
      //          new IngredientDTO
      //          {
      //              Id = ingredient.Id,
      //              Name = ingredient.Name
      //          },
      //          new IngredientDTO
      //          {
      //              Id = ingredient2.Id,
      //              Name = ingredient2.Name
      //          }
      //      };
      //
      //      mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<Ingredient>>())).Returns(ingredients);
      //
      //      var mockDateTimeProvider = new Mock<IDateTimeProvider>();
      //
      //      var id = ingredient.Id;
      //      using (var assertContext = new CMDbContext(options))
      //      {
      //          //Act & Assert
      //          var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);
      //
      //          await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllIngredientsAsync(1));
      //      }
      //  }
    }
}
