﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.IngredientServiceTests
{
    [TestClass]
    public class CreateIngredientAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIngredient()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectIngredient));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateIngredient(It.IsAny<string>()))
                       .Returns(ingredient);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.CreateIngredientAsync(ingredientDTO);

                Assert.IsInstanceOfType(result, typeof(IngredientDTO));
                
                Assert.AreEqual("Wine", result.Name);
                Assert.AreEqual(ingredientDTO.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_IngredientAlreadyExists()
        {
            var options = Utilities.GetOptions(nameof(ThrowWhen_IngredientAlreadyExists));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient2.Id,
                Name = ingredient2.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateIngredient(It.IsAny<string>()))
                       .Returns(ingredient2);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateIngredientAsync(ingredientDTO));
            }
        }
    }
}
