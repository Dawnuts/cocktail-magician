﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.IngredientServiceTests
{
    [TestClass]
    public class GetIngredientAsync_Should
    {
        [TestMethod]
        public async Task Return_CorrectIngredient()
        {
            var options = Utilities.GetOptions(nameof(Return_CorrectIngredient));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.SaveChangesAsync();
            }

            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.GetIngredientAsync(id);

                Assert.AreEqual("Wine", result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_IngredientNotFound()
        {
            var options = Utilities.GetOptions(nameof(ThrowWhen_IngredientNotFound));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetIngredientAsync(id));
            }
        }
    }
}
