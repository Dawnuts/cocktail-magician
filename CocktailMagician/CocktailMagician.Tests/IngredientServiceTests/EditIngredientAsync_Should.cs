﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.IngredientServiceTests
{
    [TestClass]
    public class EditIngredientAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIngredient_When_ValidParams()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectIngredient_When_ValidParams));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.SaveChangesAsync();
            }
            var newName = "Wine 2.0";
            ingredientDTO.Name = newName;
            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                var result = await sut.EditIngredientAsync(ingredientDTO);

                var editedIngredient = await assertContext.Ingredients.FirstAsync();

                Assert.AreEqual(newName, editedIngredient.Name);
            }
        }

        [TestMethod]
        public async Task Throw_When_IngredientNotFound()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectIngredient_When_ValidParams));

            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "Wine"
            };

            var ingredientDTO = new IngredientDTO
            {
                Id = ingredient.Id,
                Name = ingredient.Name,
            };

            var mockMapper = new Mock<IDTOMapper<Ingredient, IngredientDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Ingredient>())).Returns(ingredientDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();
            ingredientDTO.Name = "Wine 2.0";
            var id = ingredient.Id;
            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new IngredientService(assertContext, mockMapper.Object, mockFactory.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditIngredientAsync(ingredientDTO));
            }
        }
    }
}
