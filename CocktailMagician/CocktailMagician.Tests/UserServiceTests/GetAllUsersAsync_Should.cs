﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.UserServiceTests
{
    [TestClass]
   public class GetAllUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnsAllUsers()
        {

            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsAllUsers));

            var user1 = new User { UserName = "TestUser1", Id = Guid.NewGuid() };
            var user2 = new User { UserName = "TestUser2", Id = Guid.NewGuid() };

            var mockMapper = new Mock<IDTOMapper<User, UserDTO>>();
            var userDTOs = new List<UserDTO>();
            mockMapper.Setup(x => x.MapToDTO(It.IsAny<ICollection<User>>()))
                .Returns<ICollection<User>>(us => userDTOs = us.Select(u => new UserDTO
                {
                    Name = u.UserName,
                    Id = u.Id,
                    BannedUntil = u.LockoutEnd
                }).ToList());
           using(var arrangeContext =new CMDbContext(options))
            {
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.Users.AddAsync(user2);
                await arrangeContext.SaveChangesAsync();
            }

           //Act&Assert
           using(var assertContext = new CMDbContext(options))
            {
                var sut = new UserService(assertContext, mockMapper.Object);

                var result = await sut.GetAllUsersAsync();
                var resultList = result.ToList();

                var dbUsers = await assertContext.Users.ToListAsync();

                for (int i = 0; i < dbUsers.Count; i++)
                {
                    Assert.AreEqual(dbUsers[i].UserName, resultList[i].Name);
                    Assert.AreEqual(dbUsers[i].Id, resultList[i].Id);
                }
                Assert.AreEqual(2, resultList.Count);
            }
        }
    }
}
