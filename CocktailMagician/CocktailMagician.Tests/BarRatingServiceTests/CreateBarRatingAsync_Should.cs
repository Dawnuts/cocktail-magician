﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.BarRatingServiceTests
{
    [TestClass]
    public class CreateBarRatingAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBarRating()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectBarRating));

            var barId = Guid.NewGuid();

            var bar = new Bar
            {
                Id = barId,
                Name = "Terminal 1",
                Description = "Very cool",
                Phone = "111-222-333",
                Address = "Sofia, Angel Kanchev 1",
            };
            var userId = Guid.NewGuid();

            var user = new User { UserName = "TestUser1", Id = userId };
        

            var barRating = new BarRating
            {
                BarId = barId,
                UserId = userId,
                Rating = 3
            };

            var barRatingDTO = new BarRatingDTO
            {
                BarId = barRating.BarId,
                UserId = barRating.UserId,
                User = "someuser",
                Rating = barRating.Rating
            };

            var mockMapper = new Mock<IDTOMapper<BarRating, BarRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockFactory.Setup(fact => fact.CreateBarRating(It.IsAny<int>()))
                       .Returns(barRating);

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<BarRating>())).Returns(barRatingDTO);

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Bars.AddAsync(bar);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarRatingService(assertContext, mockMapper.Object, mockFactory.Object);

                var result = await sut.CreateBarRatingAsync(barRatingDTO);

                Assert.IsInstanceOfType(result, typeof(BarRatingDTO));
                Assert.AreEqual(barRatingDTO.Rating, result.Rating);
                Assert.AreEqual(barRatingDTO.UserId, result.UserId);
                Assert.AreEqual(barRatingDTO.BarId, result.BarId);
            }
        }

        [TestMethod]
        public async Task Throw_When_BarRatingDTO_IsNull()
        {
            var options = Utilities.GetOptions(nameof(Throw_When_BarRatingDTO_IsNull));

            var mockMapper = new Mock<IDTOMapper<BarRating, BarRatingDTO>>();
            var mockFactory = new Mock<IFactory>();

            using (var assertContext = new CMDbContext(options))
            {
                //Act & Assert
                var sut = new BarRatingService(assertContext, mockMapper.Object, mockFactory.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateBarRatingAsync(null));
            }
        }
    }
}
