﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class GetCocktailAsync_Should
    {
        [TestMethod]

        public async Task ReturnCorrectCocktail()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = cocktail.Id,
                Name = cocktail.Name,
                Description = cocktail.Description,
                ImgPath = cocktail.ImgPath
            };
            var id = cocktail.Id;
            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.GetCocktailAsync(id);
                var dbCocktail = await assertContext.Cocktails.FindAsync(id);


                Assert.AreEqual("TestCocktail", dbCocktail.Name);
                Assert.AreEqual("TestDescription", dbCocktail.Description);
                Assert.AreEqual("TestPath", dbCocktail.ImgPath);
                Assert.AreEqual(id, dbCocktail.Id);
            }
        }
        [TestMethod]
        public async Task ThrowsException_When_NoSuchCocktail()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsException_When_NoSuchCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = cocktail.Id,
                Name = cocktail.Name,
                Description = cocktail.Description,
                ImgPath = cocktail.ImgPath
            };
            var id = Guid.NewGuid();
            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetCocktailAsync(id));
            }
        }
    }
}
