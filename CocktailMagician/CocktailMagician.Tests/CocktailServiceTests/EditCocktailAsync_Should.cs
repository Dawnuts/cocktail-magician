﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
   public class EditCocktailAsync_Should
    {
        [TestMethod]
        public async Task ReturnEditedCocktail()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnEditedCocktail));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = cocktail.Id,
                Name = "EditedTestCocktail",
                Description = "EditedTestDescription",
                ImgPath = "EditedTestPath"
            };

            var id = cocktail.Id;

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.EditCocktailAsync(cocktailDTO);

                var dbCocktail = await assertContext.Cocktails.FindAsync(id);

                Assert.AreEqual("EditedTestCocktail", dbCocktail.Name);
                Assert.AreEqual("EditedTestDescription", dbCocktail.Description);
                Assert.AreEqual("EditedTestPath", dbCocktail.ImgPath);
                Assert.AreEqual(id, dbCocktail.Id);
            }

        }
        [TestMethod]

        public async Task ThrowsException_When_NoCocktailToEdit()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowsException_When_NoCocktailToEdit));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "EditedTestCocktail",
                Description = "EditedTestDescription",
                ImgPath = "EditedTestPath"
            };

            var id = cocktail.Id;

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditCocktailAsync(cocktailDTO));
            }
        }
    }
}
