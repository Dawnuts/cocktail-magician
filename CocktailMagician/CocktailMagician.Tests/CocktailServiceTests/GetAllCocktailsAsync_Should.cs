﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class GetAllCocktailsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCocktails()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectCocktails));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "TestCocktailTwo",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo"
            };

            var cocktailDTO1 = new CocktailDTO
            {
                Id = cocktail1.Id,
                Name = cocktail1.Name,
                Description = cocktail1.Description,
                ImgPath = cocktail1.ImgPath
            };

            var cocktailDTO2 = new CocktailDTO
            {
                Id = cocktail2.Id,
                Name = cocktail2.Name,
                Description = cocktail2.Description,
                ImgPath = cocktail2.ImgPath
            };
            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var cocktailDTOs = new List<CocktailDTO> { cocktailDTO1, cocktailDTO2 };
            var id = cocktail1.Id;
            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = cocktailDTOs
                });


            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

                //Act & Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.GetAllCocktailsAsync(1);
                var resultList = result.Collection.ToList();

                var dbList =await assertContext.Cocktails.ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
      //     [TestMethod]
      //
      // public async Task ThrowsException_When_NoCocktails()
      // {
      //     //Arrange
      //     var options = Utilities.GetOptions(nameof(ThrowsException_When_NoCocktails));
      //     var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
      //     var mockFactory = new Mock<IFactory>();
      //     var mockDateTimeProvider = new Mock<IDateTimeProvider>();
      //
      //     //Act&Assert
      //     using (var assertContext = new CMDbContext(options))
      //     {
      //         var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);
      //         await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllCocktailsAsync(1));
      //     }
      //
      // }
    }
}
