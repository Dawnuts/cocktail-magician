﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
   public class EditIngredientsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCocktailWithEditedIngredients()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCocktailWithEditedIngredients));
            var ingredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientOne",
            };

            var ingredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "TestIngredientTwo",
            };

            var editedIngredient = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "EditedIngredient",
            };

            var editedIngredient2 = new Ingredient
            {
                Id = Guid.NewGuid(),
                Name = "EditedIngredient2",
            };


            var ingredientDTO = new IngredientDTO { Id = editedIngredient.Id, Name = editedIngredient.Name };
            var ingredientDTO2 = new IngredientDTO { Id = Guid.NewGuid(), Name = editedIngredient2.Name };
            var ingredientDTOs = new List<IngredientDTO> { ingredientDTO, ingredientDTO2 };
            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = cocktail.Id,
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                Ingredients = ingredientDTOs
            };
            var cocktailIngredient = new CocktailIngredient { CocktailId = cocktail.Id, IngredientId = ingredient.Id };
            var cocktailIngredient2 = new CocktailIngredient { CocktailId = cocktail.Id, IngredientId = ingredient2.Id };
            var cocktailIngredients = new List<CocktailIngredient> { cocktailIngredient, cocktailIngredient2 };

            cocktail.Ingredients = cocktailIngredients;

            var id = cocktail.Id;

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            mockFactory.SetupSequence(x => x.CreateCocktailIngredient(It.IsAny<Guid>(), It.IsAny<Guid>()))
                .Returns(new CocktailIngredient { CocktailId = id, IngredientId = editedIngredient.Id })
                .Returns(new CocktailIngredient { CocktailId = id, IngredientId = editedIngredient2.Id });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Ingredients.AddAsync(ingredient);
                await arrangeContext.Ingredients.AddAsync(ingredient2);
                await arrangeContext.Ingredients.AddAsync(editedIngredient);
                await arrangeContext.Ingredients.AddAsync(editedIngredient2);
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.CocktailIngredients.AddRangeAsync(cocktailIngredients);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.EditIngredientsAsync(cocktailDTO);

                var dbCocktail = await assertContext.Cocktails.FindAsync(id);

                Assert.IsTrue(assertContext.CocktailIngredients.Any(ci => ci.CocktailId == id && ci.IngredientId == editedIngredient.Id));
                Assert.IsTrue(assertContext.CocktailIngredients.Any(ci => ci.CocktailId == id && ci.IngredientId == editedIngredient2.Id));
                Assert.IsFalse(assertContext.CocktailIngredients.Any(ci => ci.CocktailId == id && ci.IngredientId == ingredient.Id));
                Assert.IsFalse(assertContext.CocktailIngredients.Any(ci => ci.CocktailId == id && ci.IngredientId == ingredient2.Id));

                Assert.IsTrue(dbCocktail.Ingredients.Any(ci => ci.CocktailId == id && ci.IngredientId == editedIngredient.Id));
                Assert.IsTrue(dbCocktail.Ingredients.Any(ci => ci.CocktailId == id && ci.IngredientId == editedIngredient2.Id));
                Assert.IsFalse(dbCocktail.Ingredients.Any(ci => ci.CocktailId == id && ci.IngredientId == ingredient.Id));
                Assert.IsFalse(dbCocktail.Ingredients.Any(ci => ci.CocktailId == id && ci.IngredientId == ingredient2.Id));
                
            }
        }
        [TestMethod]
        public async Task ThrowException_When_NoCocktailToEditIngredients()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_NoCocktailToEditIngredients));

            var cocktail = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };

            var cocktailDTO = new CocktailDTO
            {
                Id = Guid.NewGuid(),
                Name = "EditedTestCocktail",
                Description = "EditedTestDescription",
                ImgPath = "EditedTestPath"
            };

            var id = cocktail.Id;

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();

            mockMapper.Setup(x => x.MapToDTO(It.IsAny<Cocktail>())).Returns(cocktailDTO);

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.EditIngredientsAsync(cocktailDTO));
            }
        }
    }
}
