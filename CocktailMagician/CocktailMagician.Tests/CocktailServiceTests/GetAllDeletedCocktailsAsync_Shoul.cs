﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class GetAllDeletedCocktailsAsync_Shoul
    {
        [TestMethod]
        public async Task ReturnAllDeletedCocktails()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnAllDeletedCocktails));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "TestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                IsDeleted = true
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "TestCocktailTwo",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo",
                IsDeleted = true
            };

            var cocktailDTO1 = new CocktailDTO
            {
                Id = cocktail1.Id,
                Name = cocktail1.Name,
                Description = cocktail1.Description,
                ImgPath = cocktail1.ImgPath
            };

            var cocktailDTO2 = new CocktailDTO
            {
                Id = cocktail2.Id,
                Name = cocktail2.Name,
                Description = cocktail2.Description,
                ImgPath = cocktail2.ImgPath
            };
            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var cocktailDTOs = new List<CocktailDTO> { cocktailDTO1, cocktailDTO2 };
            var id = cocktail1.Id;
            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = cocktailDTOs
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.GetAllDeletedCocktailsAsync(1);
                var resultList = result.Collection.ToList();

                var dbDeletedCocktails = await assertContext.Cocktails
                    .Where(c => c.IsDeleted == true).ToListAsync();

                for (int i = 0; i < dbDeletedCocktails.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbDeletedCocktails[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbDeletedCocktails[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbDeletedCocktails[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbDeletedCocktails[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
    }

}
