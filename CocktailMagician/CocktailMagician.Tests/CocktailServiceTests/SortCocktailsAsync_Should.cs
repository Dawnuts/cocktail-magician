﻿using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Factories;
using CocktailMagician.Services.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocktailMagician.Tests.CocktailServiceTests
{
    [TestClass]
    public class SortCocktailsAsync_Should
    {
        [TestMethod]
        public async Task ReturnSortedCocktailsByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnSortedCocktailsByName));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo"
            };

            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath
                    }).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.SortCocktailsAsync("name", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails.OrderBy(c => c.Name)
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
        [TestMethod]
        public async Task ReturnsCocktailsSortedByNameDesc()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsCocktailsSortedByNameDesc));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath"
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo"
            };

            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath
                    }).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.SortCocktailsAsync("name_desc", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails.OrderByDescending(c => c.Name)
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
        [TestMethod]
        public async Task ReturnsCocktailsSortedByRating()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsCocktailsSortedByRating));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                AverageRating = 3
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo",
                AverageRating = 4.2
            };

            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath,
                        AverageRating = c.AverageRating
                    }).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.SortCocktailsAsync("rating", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails.OrderBy(c => c.AverageRating)
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
        [TestMethod]
        public async Task ReturnsCocktailsSortedByRatingDesc()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnsCocktailsSortedByRatingDesc));

            var cocktail1 = new Cocktail
            {
                Id = Guid.NewGuid(),
                Name = "BTestCocktail",
                Description = "TestDescription",
                ImgPath = "TestPath",
                AverageRating = 3
            };
            var cocktail2 = new Cocktail()
            {

                Id = Guid.NewGuid(),
                Name = "ATestCocktail",
                Description = "TestDescriptionTwo",
                ImgPath = "TestPathTwo",
                AverageRating = 4.2
            };

            var cocktails = new List<Cocktail> { cocktail1, cocktail2 };

            var mockMapper = new Mock<IDTOMapper<Cocktail, CocktailDTO>>();
            var mockFactory = new Mock<IFactory>();


            mockMapper.Setup(x => x.MapToDTO(It.IsAny<PaginatedList<Cocktail>>()))
                .Returns<PaginatedList<Cocktail>>(pl => new PageTransferInfo<CocktailDTO>
                {
                    HasNextPage = pl.HasNextPage,
                    HasPreviousPage = pl.HasPreviousPage,
                    TotalPages = pl.TotalPages,
                    PageIndex = pl.PageIndex,
                    Collection = pl.Select(c => new CocktailDTO
                    {
                        Name = c.Name,
                        Id = c.Id,
                        Description = c.Description,
                        ImgPath = c.ImgPath,
                        AverageRating = c.AverageRating
                    }).ToList()
                });

            var mockDateTimeProvider = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new CMDbContext(options))
            {
                await arrangeContext.Cocktails.AddAsync(cocktail1);
                await arrangeContext.Cocktails.AddAsync(cocktail2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new CMDbContext(options))
            {
                var sut = new CocktailService(assertContext, mockFactory.Object, mockMapper.Object, mockDateTimeProvider.Object);

                var result = await sut.SortCocktailsAsync("rating_desc", 1);
                var resultList = result.Collection.ToList();

                var dbList = await assertContext.Cocktails.OrderByDescending(c => c.AverageRating)
                    .ToListAsync();

                for (int i = 0; i < dbList.Count; i++)
                {
                    Assert.AreEqual(resultList[i].Name, dbList[i].Name);
                    Assert.AreEqual(resultList[i].Description, dbList[i].Description);
                    Assert.AreEqual(resultList[i].ImgPath, dbList[i].ImgPath);
                    Assert.AreEqual(resultList[i].Id, dbList[i].Id);
                }
                Assert.AreEqual(2, result.Collection.Count);
            }
        }
    }
}
