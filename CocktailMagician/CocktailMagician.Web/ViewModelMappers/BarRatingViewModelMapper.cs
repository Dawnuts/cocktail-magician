﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class BarRatingViewModelMapper : IViewModelMapper<BarRatingDTO, BarRatingViewModel>
    {
        public BarRatingDTO MapToDTO(BarRatingViewModel viewModel)
        {
            var barRatingDTO = new BarRatingDTO
            {
                BarId = viewModel.BarId,
                Rating = viewModel.Rating,
                UserId = viewModel.UserId
            } ?? throw new ArgumentNullException();

            return barRatingDTO;
        }

        public BarRatingViewModel MapToViewModel(BarRatingDTO barRatingDTO)
        {
            if (barRatingDTO == null)
            {
                throw new ArgumentNullException();
            }

            var barRatingViewModel = new BarRatingViewModel
            {
                Bar = barRatingDTO.Bar,
                BarId = barRatingDTO.BarId,
                Rating = barRatingDTO.Rating,
                User = barRatingDTO.User,
                UserId = barRatingDTO.UserId
            };

            return barRatingViewModel;
        }

        public ICollection<BarRatingViewModel> MapToViewModel(ICollection<BarRatingDTO> barRatingsDTO)
        {
            var barRatingsViewModel = barRatingsDTO.Select(ratingDTO => MapToViewModel(ratingDTO)).ToList();

            return barRatingsViewModel;
        }

        public PageTransferInfo<BarRatingViewModel> MapToViewModel(PageTransferInfo<BarRatingDTO> ratingsPage)
        {
            var vmPage = new PageTransferInfo<BarRatingViewModel>();
            vmPage.TotalPages = ratingsPage.TotalPages;
            vmPage.PageIndex = ratingsPage.PageIndex;
            vmPage.HasNextPage = ratingsPage.HasNextPage;
            vmPage.HasPreviousPage = ratingsPage.HasPreviousPage;
            vmPage.Collection = ratingsPage.Collection.Select(ratingDTO => MapToViewModel(ratingDTO)).ToList();

            return vmPage;
        }
    }
}
