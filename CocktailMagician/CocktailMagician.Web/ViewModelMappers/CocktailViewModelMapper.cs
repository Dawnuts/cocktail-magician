﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class CocktailViewModelMapper : IViewModelMapper<CocktailDTO, CocktailViewModel>
    {
        public CocktailDTO MapToDTO(CocktailViewModel viewModel)
        {
            var cocktailDTO = new CocktailDTO
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Description = viewModel.Description,
                AverageRating = viewModel.AverageRating,
                ImgPath = viewModel.ImgPath,
                Ingredients = viewModel.Ingredients
                 .Select(ivm => new IngredientDTO { Id = ivm.Id, Name = ivm.Name })
                 .ToList(),
            } ?? throw new ArgumentNullException();

            return cocktailDTO;
        }

        public CocktailViewModel MapToViewModel(CocktailDTO cocktailDTO)
        {
            if (cocktailDTO == null)
            {
                throw new ArgumentNullException();
            }

            var cocktailViewModel = new CocktailViewModel
            {
                Id = cocktailDTO.Id,
                Name = cocktailDTO.Name,
                Description = cocktailDTO.Description,
                AverageRating = cocktailDTO.AverageRating,
                ImgPath = cocktailDTO.ImgPath,
                Ingredients = cocktailDTO.Ingredients
                .Select(ingredientDTO => new IngredientViewModel { Id = ingredientDTO.Id, Name = ingredientDTO.Name, ImgPath = ingredientDTO.ImgPath })
                .ToList(),
                Comments = cocktailDTO.Comments.Select(co => new CocktailCommentViewModel
                {
                    Id = co.Id,
                    Text = co.Text,
                    CocktailId = cocktailDTO.Id,
                    User = co.User,
                    UserId = co.UserId
                }).ToList(),
                Bars = cocktailDTO.Bars.Select(b => new BarViewModel { Name = b.Name, Id = b.Id, ImgPath = b.ImgPath }).ToList()

            };
            
            return cocktailViewModel;
        }

        public ICollection<CocktailViewModel> MapToViewModel(ICollection<CocktailDTO> cocktailsDTO)
        {
            var cocktailsViewModel = cocktailsDTO.Select(cocktailDTO => MapToViewModel(cocktailDTO)).ToList();
            
            return cocktailsViewModel;
        }

        public PageTransferInfo<CocktailViewModel> MapToViewModel(PageTransferInfo<CocktailDTO> cocktailsPage)
        {
            var vmPage = new PageTransferInfo<CocktailViewModel>();
            vmPage.TotalPages = cocktailsPage.TotalPages;
            vmPage.PageIndex = cocktailsPage.PageIndex;
            vmPage.HasNextPage = cocktailsPage.HasNextPage;
            vmPage.HasPreviousPage = cocktailsPage.HasPreviousPage;
            vmPage.Collection = cocktailsPage.Collection.Select(cocktailDTO => MapToViewModel(cocktailDTO)).ToList();

            return vmPage;
        }
    }
}
