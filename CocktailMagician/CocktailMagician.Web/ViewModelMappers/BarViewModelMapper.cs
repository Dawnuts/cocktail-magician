﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class BarViewModelMapper : IViewModelMapper<BarDTO, BarViewModel>
    {
        public BarDTO MapToDTO(BarViewModel viewModel)
        {
            var barDTO = new BarDTO
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Description = viewModel.Description,
                ImgPath = viewModel.ImgPath,
                Address = viewModel.Address,
                AverageRating = viewModel.AverageRating,
                Cocktails = viewModel.Cocktails.Select(cvm => new CocktailDTO { Id = cvm.Id, Name = cvm.Name }).ToList(),
                Phone = viewModel.Phone
            } ?? throw new ArgumentNullException();

            return barDTO;
        }

        public BarViewModel MapToViewModel(BarDTO barDTO)
        {
            if (barDTO == null)
            {
                throw new ArgumentNullException();
            }

            var barViewModel = new BarViewModel
            {
                Id = barDTO.Id,
                Name = barDTO.Name,
                Description = barDTO.Description,
                ImgPath = barDTO.ImgPath,
                Address = barDTO.Address,
                AverageRating = barDTO.AverageRating,
                Cocktails = barDTO.Cocktails.Select(cocktailDTO => new CocktailViewModel { Id = cocktailDTO.Id, Name = cocktailDTO.Name, ImgPath = cocktailDTO.ImgPath })
                                            .ToList(),
                Comments = barDTO.Comments.Select(co => new BarCommentViewModel
                {
                    BarId = co.BarId,
                    Id = co.Id,
                    Text = co.Text,
                    User = co.User,
                    UserId = co.UserId
                }).ToList(),
                Phone = barDTO.Phone
            };
            
            return barViewModel;
        }

        public ICollection<BarViewModel> MapToViewModel(ICollection<BarDTO> barsDTO)
        {
            var barsViewModel = barsDTO.Select(barDTO => MapToViewModel(barDTO)).ToList();
            
            return barsViewModel;
        }

        public PageTransferInfo<BarViewModel> MapToViewModel(PageTransferInfo<BarDTO> barsDTOPage)
        {
            var vmPage = new PageTransferInfo<BarViewModel>();
            vmPage.TotalPages = barsDTOPage.TotalPages;
            vmPage.PageIndex = barsDTOPage.PageIndex;
            vmPage.HasNextPage = barsDTOPage.HasNextPage;
            vmPage.HasPreviousPage = barsDTOPage.HasPreviousPage;
            vmPage.Collection = barsDTOPage.Collection.Select(barDTO => MapToViewModel(barDTO)).ToList();

            return vmPage;
        }
    }
}
