﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class CocktailCommentViewModelMapper : IViewModelMapper<CocktailCommentDTO, CocktailCommentViewModel>
    {
        public CocktailCommentDTO MapToDTO(CocktailCommentViewModel viewModel)
        {
            var cocktailCommentDTO = new CocktailCommentDTO
            {
                Id = viewModel.Id,
                CocktailId = viewModel.CocktailId,
                Text = viewModel.Text,
                UserId = viewModel.UserId
            } ?? throw new ArgumentNullException();

            return cocktailCommentDTO;
        }

        public CocktailCommentViewModel MapToViewModel(CocktailCommentDTO cocktailCommentDTO)
        {
            if (cocktailCommentDTO == null)
            {
                throw new ArgumentNullException();
            }

            var cocktailCommentViewModel = new CocktailCommentViewModel
            {
                Id = cocktailCommentDTO.Id,
                Cocktail = cocktailCommentDTO.Cocktail,
                CocktailId = cocktailCommentDTO.CocktailId,
                Text = cocktailCommentDTO.Text,
                User = cocktailCommentDTO.User,
                UserId = cocktailCommentDTO.UserId
            };

            return cocktailCommentViewModel;
        }

        public ICollection<CocktailCommentViewModel> MapToViewModel(ICollection<CocktailCommentDTO> cocktailCommentsDTO)
        {
            var cocktailCommentsViewModel = cocktailCommentsDTO.Select(commentDTO => MapToViewModel(commentDTO)).ToList();

            return cocktailCommentsViewModel;
        }

        public PageTransferInfo<CocktailCommentViewModel> MapToViewModel(PageTransferInfo<CocktailCommentDTO> commentsPage)
        {
            var vmPage = new PageTransferInfo<CocktailCommentViewModel>();
            vmPage.TotalPages = commentsPage.TotalPages;
            vmPage.PageIndex = commentsPage.PageIndex;
            vmPage.HasNextPage = commentsPage.HasNextPage;
            vmPage.HasPreviousPage = commentsPage.HasPreviousPage;
            vmPage.Collection = commentsPage.Collection.Select(commentDTO => MapToViewModel(commentDTO)).ToList();

            return vmPage;
        }
    }
}
