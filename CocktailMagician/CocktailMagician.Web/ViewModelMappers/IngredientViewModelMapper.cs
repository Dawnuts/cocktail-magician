﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class IngredientViewModelMapper : IViewModelMapper<IngredientDTO, IngredientViewModel>
    {
        public IngredientDTO MapToDTO(IngredientViewModel viewModel)
        {
            var ingredientDTO = new IngredientDTO
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                ImgPath = viewModel.ImgPath
            } ?? throw new ArgumentNullException();

            return ingredientDTO;
        }

        public IngredientViewModel MapToViewModel(IngredientDTO ingredientDTO)
        {
            if (ingredientDTO == null)
            {
                throw new ArgumentNullException();
            }

            var ingredientViewModel = new IngredientViewModel
            {
                Id = ingredientDTO.Id,
                Name = ingredientDTO.Name,
                ImgPath = ingredientDTO.ImgPath,
                Cocktails = ingredientDTO.Cocktails
                .Select(c => new CocktailViewModel { Id = c.Id, Name = c.Name, ImgPath = c.ImgPath}).ToList()
            };
            
            return ingredientViewModel;
        }
        public ICollection<IngredientViewModel> MapToViewModel(ICollection<IngredientDTO> ingredientsDTO)
        {
            var ingredientsViewModel = ingredientsDTO.Select(ingredientDTO => MapToViewModel(ingredientDTO)).ToList();
            
            return ingredientsViewModel;
        }

        public PageTransferInfo<IngredientViewModel> MapToViewModel(PageTransferInfo<IngredientDTO> ingredientsPage)
        {
            var vmPage = new PageTransferInfo<IngredientViewModel>();
            vmPage.TotalPages = ingredientsPage.TotalPages;
            vmPage.PageIndex = ingredientsPage.PageIndex;
            vmPage.HasNextPage = ingredientsPage.HasNextPage;
            vmPage.HasPreviousPage = ingredientsPage.HasPreviousPage;
            vmPage.Collection = ingredientsPage.Collection.Select(ingredientDTO => MapToViewModel(ingredientDTO)).ToList();

            return vmPage;
        }
    }
}
