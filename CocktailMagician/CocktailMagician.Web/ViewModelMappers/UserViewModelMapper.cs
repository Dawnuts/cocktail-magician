﻿using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.ViewModelMappers
{
    public class UserViewModelMapper : IViewModelMapper<UserDTO, UserViewModel>
    {
        public UserDTO MapToDTO(UserViewModel userViewModel)
        {
            var userDTO = new UserDTO
            {
                Name = userViewModel.Name,
                Id = userViewModel.Id,
                BannedUntil = userViewModel.BannedUntil
            } ?? throw new ArgumentNullException();
            return userDTO;
        }

        public UserViewModel MapToViewModel(UserDTO userDTO)
        {
            var userViewModel = new UserViewModel
            {
                Name = userDTO.Name,
                Id = userDTO.Id,
                IsAdmin = userDTO.IsAdmin,
                BannedUntil = userDTO.BannedUntil,
            } ?? throw new ArgumentNullException();

            return userViewModel;
        }

        public ICollection<UserViewModel> MapToViewModel(ICollection<UserDTO> userDTOs)
        {
            var userViewModels = userDTOs.Select(u => MapToViewModel(u)).ToList();
            return userViewModels;
        }

        public PageTransferInfo<UserViewModel> MapToViewModel(PageTransferInfo<UserDTO> userPage)
        {
            var vmPage = new PageTransferInfo<UserViewModel>();
            vmPage.TotalPages = userPage.TotalPages;
            vmPage.PageIndex = userPage.PageIndex;
            vmPage.HasNextPage = userPage.HasNextPage;
            vmPage.HasPreviousPage = userPage.HasPreviousPage;
            vmPage.Collection = userPage.Collection.Select(u=> MapToViewModel(u)).ToList();

            return vmPage;
        }
    }
}
