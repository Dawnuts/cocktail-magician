﻿using CocktailMagician.Web.Areas.Member.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Models
{
    [BindProperties(SupportsGet = true)]
    public class CocktailViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Cocktail Name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [DisplayName("Cocktail Description")]
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        public string ImgPath { get; set; }
        public ICollection<IngredientViewModel> Ingredients { get; set; } = new List<IngredientViewModel>();
        public ICollection<CocktailCommentViewModel> Comments { get; set; } = new List<CocktailCommentViewModel>();
        public ICollection<CocktailRatingViewModel> Ratings { get; set; }
        public ICollection<BarViewModel> Bars { get; set; } = new List<BarViewModel>();
        public double AverageRating { get; set; }
    }
}
