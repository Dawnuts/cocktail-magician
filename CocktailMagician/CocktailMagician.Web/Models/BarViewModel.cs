﻿using CocktailMagician.Web.Areas.Member.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Models
{
    [BindProperties(SupportsGet = true)]
    public class BarViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Bar Name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [DisplayName("Bar Description")]
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        public string ImgPath { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [MinLength(9)]
        public string Phone { get; set; }
        public ICollection<CocktailViewModel> Cocktails { get; set; } = new List<CocktailViewModel>();
        public ICollection<BarCommentViewModel> Comments { get; set; } = new List<BarCommentViewModel>();
        public ICollection<BarRatingViewModel> Ratings { get; set; }
        public double AverageRating { get; set; }
    }
}
