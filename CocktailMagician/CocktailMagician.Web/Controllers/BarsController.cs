﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Web.Models;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Services.Providers;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using CocktailMagician.Web.Utilities.Constants;
using NToastNotify;

namespace CocktailMagician.Web.Controllers
{
    public class BarsController : Controller
    {

        private readonly IBarService barService;
        private readonly ICocktailService cocktailService;
        private readonly IViewModelMapper<BarDTO, BarViewModel> viewModelMapper;
        private readonly IViewModelMapper<CocktailDTO, CocktailViewModel> cocktailMapper;
        private readonly IImageStorageProvider imageStorage;
        private readonly IToastNotification toastNotification;

        public BarsController(IBarService barService, ICocktailService cocktailService,
            IViewModelMapper<BarDTO, BarViewModel> viewModelMapper,
            IViewModelMapper<CocktailDTO, CocktailViewModel> cocktailMapper,
            IImageStorageProvider imageStorage, IToastNotification toastNotification)
        {
            this.barService = barService;
            this.cocktailService = cocktailService;
            this.viewModelMapper = viewModelMapper;
            this.cocktailMapper = cocktailMapper;
            this.imageStorage = imageStorage;
            this.toastNotification = toastNotification;
        }

        // GET: Bars
        public async Task<IActionResult> Index(string filterOption, string sortOption, int page = 1)
        {
            PageTransferInfo<BarDTO> bars;
            ViewBag.CurrentFilter = filterOption;
            ViewBag.CurrentSort = sortOption;

            ViewBag.SortByName = sortOption == "name" ? "name_desc" : "name";
            ViewBag.SortByRating = sortOption == "rating" ? "rating_desc" : "rating";
            ViewBag.SortByAddress = sortOption == "address" ? "address_desc" : "address";

            if (!String.IsNullOrEmpty(filterOption))
            {
                bars = await this.barService.FilterBarsAsync(filterOption, page);
            }
            else if (!String.IsNullOrEmpty(sortOption))
            {
                bars = await this.barService.SortBarsAsync(sortOption, page);
            }
            else
            {
                bars = await this.barService.GetAllBarsAsync(page);
            }

            var barsViewModel = this.viewModelMapper.MapToViewModel(bars);

            return View(barsViewModel);
        }

        // GET: Bars/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            try
            {
                var bar = await this.barService.GetBarAsync(id.Value);



                var barViewModel = this.viewModelMapper.MapToViewModel(bar);

                return View(barViewModel);
            }
            catch
            {
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                return RedirectToAction("Index");
            }
        }

        // GET: Bars/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bars/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] BarCreationViewModel barCreationModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (barCreationModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(barCreationModel.File, "bars");
                        barCreationModel.Bar.ImgPath = imagePath;
                    }
                    else
                    {
                        barCreationModel.Bar.ImgPath = ConstantsHolder.DefaultBarImg;
                    }
                    var barDTO = this.viewModelMapper.MapToDTO(barCreationModel.Bar);

                    await this.barService.CreateBarAsync(barDTO);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CreatedBar);
                    return RedirectToAction(nameof(Index));

                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.BarExists);
                    return View(barCreationModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(barCreationModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(barCreationModel);
        }

        // GET: Bars/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var bar = await this.barService.GetBarAsync(id.Value);

                if (bar == null)
                {
                    return NotFound();
                }

                var barViewModel = this.viewModelMapper.MapToViewModel(bar);
                var barEditModel = new BarCreationViewModel { Bar = barViewModel };
                return View(barEditModel);
            }
            catch
            {
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                return RedirectToAction("Index");
            }
        }

        // POST: Bars/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind] BarCreationViewModel barEditModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    if (barEditModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(barEditModel.File, "bars");
                        barEditModel.Bar.ImgPath = imagePath;
                    }
                    var newName = barEditModel.Bar.Name;
                    var newDescription = barEditModel.Bar.Description;
                    var newPhone = barEditModel.Bar.Phone;
                    var newAddress = barEditModel.Bar.Address;
                    var imgPath = barEditModel.Bar.ImgPath;
                    await this.barService.EditBarAsync(barEditModel.Bar.Id, newName, newDescription, newPhone, newAddress, imgPath);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.BarEdited);
                    return RedirectToAction(nameof(Index));
                }
                catch (ArgumentNullException)
                {
                    return NotFound();
                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.BarExists);
                    return View(barEditModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(barEditModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(barEditModel);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddRemoveCocktails(Guid? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var bar = await this.barService.GetBarAsync(id.Value);

                if (bar == null)
                {
                    return NotFound();
                }

                var barViewModel = this.viewModelMapper.MapToViewModel(bar);
                var cocktails = await this.cocktailService.GetAllCocktailsAsync(1, int.MaxValue);
                var barCocktailsModel = new BarCocktailsViewModel
                {
                    Bar = barViewModel,
                    AllCocktails = cocktails.Collection.Select(c => cocktailMapper.MapToViewModel(c)).ToList()
                };

                return View(barCocktailsModel);
            }
            catch
            {
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRemoveCocktails([Bind] BarCocktailsViewModel barCocktailsViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var cocktailViewModels = barCocktailsViewModel.CocktailIds
                        .Select(c => new CocktailViewModel { Id = c }).ToList();
                    barCocktailsViewModel.Bar.Cocktails = cocktailViewModels;
                    var barDTO = this.viewModelMapper.MapToDTO(barCocktailsViewModel.Bar);
                    var bar = await this.barService.AddAndRemoveCocktailsAsync(barDTO);

                    var barViewModel = this.viewModelMapper.MapToViewModel(bar);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.BarEdited);
                    return RedirectToAction("Details", new { id = barViewModel.Id });
                }
                catch
                {
                    return NotFound();
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(barCocktailsViewModel);
        }

        // GET: Bars/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
            var bar = await this.barService.GetBarAsync(id.Value);

            var barViewModel = this.viewModelMapper.MapToViewModel(bar);

            return View(barViewModel);
            }
            catch
            {
                return NotFound();
            }
        }

        // POST: Bars/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var result = await this.barService.DeleteBarAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.BarDeleted);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.BarNotDeleted);

            return RedirectToAction(nameof(Index));
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Deleted(int page = 1)
        {
            var bars = await this.barService.GetAllDeletedBarsAsync(page);
            var barsViewModel = this.viewModelMapper.MapToViewModel(bars);
            return View(barsViewModel);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DetailsDeleted(Guid? id)
        {
            try
            {
                var bar = await this.barService.GetDeletedBarAsync(id.Value);

                var barViewModel = this.viewModelMapper.MapToViewModel(bar);

                return View(barViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Restore(Guid id)
        {
            var result = await this.barService.RestoreBarAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.BarRestored);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.BarNotRestored);

            return RedirectToAction(nameof(Index));
        }
    }
}
