﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Utilities.Constants
{
    internal static class ConstantsHolder
    {
        internal const string DefaultBarImg = "/images/bars/default-bar.jpeg";
        internal const string DefaultCocktailImg = "/images/cocktails/default-cocktail.jpg";
        internal const string DefaultIngredientsImg = "/images/ingredients/default-ingredient.jpg";
        internal const string GeneralErrorMessage = "Something was not right.";
        internal const string CreatedBar = "You've created a new bar!";
        internal const string BarExists = "A bar with this name already exists!";
        internal const string InvalidInput = "Invalid input.Try again.";
        internal const string InvalidFiletype = "The file you uploaded was not an image file";
        internal const string BarDeleted = "Bar successfully deleted.";
        internal const string BarNotDeleted = "Bar could not be deleted.";
        internal const string BarRestored = "Bar successfully restored.";
        internal const string BarNotRestored = "Bar could not be restored.";
        internal const string CocktailExists = "A cocktail with this name already exists!";
        internal const string CreatedCocktail = "You've created a new cocktail!";
        internal const string BarEdited = "Bar successfully edited.";
        internal const string CocktailEdited = "Cocktail successfully edited.";
        internal const string CocktailDeleted = "Cocktail successfully deleted.";
        internal const string CocktailNotDeleted = "Cocktail could not be deleted.";
        internal const string CocktailRestored = "Cocktail successfully restored.";
        internal const string CocktailNotRestored = "Cocktail could not be restored.";
        internal const string InvalidComment = "Comment must be less than 500 symbols long.";
        internal const string CommentAdded = "Comment added!";
        internal const string CommentDelted = "Comment successfully deleted.";
        internal const string CommentNotDeleted = "Comment could not be deleted.";
        internal const string RatingAdded = "Rating added!";
        internal const string UserBanned = "User successfully banned.";
        internal const string UserNotBanned = "User could not be banned.";
        internal const string UserUnbanned = "User ban lifted.";
        internal const string UserNotUnbanned = "User ban could not be lifted.";
        internal const string IngredientExists = "Ingredient with this name already exists.";
        internal const string IngredientCreated = "Ingredient successfully created!";
        internal const string IngredientEdited = "Ingredient successfully edited!";
        internal const string IngredientDeleted = "Ingredient successfully deleted.";
        internal const string IngredientNotDeleted = "Ingredient could not be deleted.";

    }
}
