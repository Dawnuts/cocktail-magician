﻿using CocktailMagician.Data.Models;
using CocktailMagician.Services.DTOMappers;
using CocktailMagician.Services.DTOMappers.Contracts;
using CocktailMagician.Services.DTOs;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Utilities.Registration
{
    public static class DTOMappersRegistration
    {
        public static IServiceCollection AddDTOMappers(this IServiceCollection services)
        {
            services.AddScoped<IDTOMapper<Ingredient, IngredientDTO>, IngredientMapper>();

            services.AddScoped<IDTOMapper<Cocktail, CocktailDTO>, CocktailMapper>();
            services.AddScoped<IDTOMapper<CocktailComment, CocktailCommentDTO>, CocktailCommentMapper>();
            services.AddScoped<IDTOMapper<CocktailRating, CocktailRatingDTO>, CocktailRatingMapper>();

            services.AddScoped<IDTOMapper<Bar, BarDTO>, BarMapper>();
            services.AddScoped<IDTOMapper<BarComment, BarCommentDTO>, BarCommentMapper>();
            services.AddScoped<IDTOMapper<BarRating, BarRatingDTO>, BarRatingMapper>();
            services.AddScoped<IDTOMapper<User, UserDTO>, UserMapper>();

            return services;
        }
    }
}
