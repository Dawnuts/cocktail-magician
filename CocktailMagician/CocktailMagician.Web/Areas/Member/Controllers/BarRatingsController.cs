﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Utilities.Constants;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace CocktailMagician.Web.Areas.Member.Controllers
{
    [Area("Member")]
    [Authorize]
    public class BarRatingsController : Controller
    {
        private readonly IBarRatingService barRatingService;
        private readonly IViewModelMapper<BarRatingDTO, BarRatingViewModel> viewModelMapper;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public BarRatingsController(IBarRatingService barRatingService,
                                     IViewModelMapper<BarRatingDTO, BarRatingViewModel> viewModelMapper,
                                     UserManager<User> userManager, IToastNotification toastNotification)
        {
            this.barRatingService = barRatingService;
            this.viewModelMapper = viewModelMapper;
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] BarRatingViewModel barRatingViewModel)
        {

            if (ModelState.IsValid)
            {
                var currentUser = await userManager.GetUserAsync(HttpContext.User);
                barRatingViewModel.UserId = currentUser.Id;

                var barRatingDTO = this.viewModelMapper.MapToDTO(barRatingViewModel);

                await this.barRatingService.CreateBarRatingAsync(barRatingDTO);

                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.RatingAdded);
                return RedirectToAction("Details", "Bars", new { area = "", id = barRatingViewModel.BarId });
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(barRatingViewModel);
        }
    }
}