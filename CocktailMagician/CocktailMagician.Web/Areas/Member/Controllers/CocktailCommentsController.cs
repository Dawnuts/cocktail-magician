﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Member.Models;
using CocktailMagician.Web.Utilities.Constants;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace CocktailMagician.Web.Areas.Member.Controllers
{
    [Area("Member")]
    [Authorize]
    public class CocktailCommentsController : Controller
    {
        private readonly ICocktailCommentService service;
        private readonly IViewModelMapper<CocktailCommentDTO, CocktailCommentViewModel> viewModelMapper;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public CocktailCommentsController(ICocktailCommentService service,
                                     IViewModelMapper<CocktailCommentDTO, CocktailCommentViewModel> viewModelMapper,
                                     UserManager<User> userManager, IToastNotification toastNotification)
        {
            this.service = service;
            this.viewModelMapper = viewModelMapper;
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] CocktailCommentViewModel cocktailCommentViewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var currentUser = await userManager.GetUserAsync(HttpContext.User);
                    cocktailCommentViewModel.UserId = currentUser.Id;

                    var cocktailCommentDTO = this.viewModelMapper.MapToDTO(cocktailCommentViewModel);
                    await this.service.CreateCocktailCommentAsync(cocktailCommentDTO);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CommentAdded);
                    return RedirectToAction("Details", "Cocktails", new { area = "", id = cocktailCommentViewModel.CocktailId });
                }
                catch
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.GeneralErrorMessage);
                    return RedirectToAction( "Details", "Cocktails", new { id = cocktailCommentViewModel.CocktailId, area = "" });
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidComment);
            return RedirectToAction( "Details", "Cocktails", new { id = cocktailCommentViewModel.CocktailId, area = "" });
        }

        public async Task<IActionResult> Edit(CocktailCommentViewModel cocktailCommentViewModel)
        {
            var currentUser = await userManager.GetUserAsync(HttpContext.User);

            if (currentUser.Id != cocktailCommentViewModel.UserId)
            {
                return View(cocktailCommentViewModel);
            }
            return RedirectToAction("Details", "Cocktails", new { area = "", id = cocktailCommentViewModel.CocktailId });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind] CocktailCommentViewModel cocktailCommentViewModel)
        {
            if (id != cocktailCommentViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var newComment = cocktailCommentViewModel.Text;
                    var comment = await this.service.EditCocktailCommentAsync(id, newComment);
                    cocktailCommentViewModel = this.viewModelMapper.MapToViewModel(comment);

                    return RedirectToAction("Index", "Cocktails");
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(cocktailCommentViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await this.service.DeleteCocktailCommentAsync(id);

            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.CommentDelted);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.CommentNotDeleted);

            return RedirectToAction("Index", "Cocktails", new { area = "" });
        }
    }
}
