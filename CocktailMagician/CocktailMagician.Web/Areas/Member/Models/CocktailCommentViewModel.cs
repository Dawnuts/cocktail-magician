﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Member.Models
{
    [BindProperties(SupportsGet = true)]
    public class CocktailCommentViewModel
    {
        public Guid Id { get; set; }
        public Guid CocktailId { get; set; }
        public string Cocktail { get; set; }
        [Required]
        [StringLength(500)]
        public string Text { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}
