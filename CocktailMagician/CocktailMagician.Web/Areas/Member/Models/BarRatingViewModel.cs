﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Member.Models
{
    [BindProperties(SupportsGet = true)]
    public class BarRatingViewModel
    {
        public Guid BarId { get; set; }
        public string Bar { get; set; }
        public int Rating { get; set; }
        public Guid UserId { get; set; }
        public string User { get; set; }
    }
}
