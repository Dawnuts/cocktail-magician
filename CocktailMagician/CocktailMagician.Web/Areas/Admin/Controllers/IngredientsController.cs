﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Services.Contracts;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Services.Providers;
using CocktailMagician.Web.Utilities.Constants;
using NToastNotify;

namespace CocktailMagician.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class IngredientsController : Controller
    {
        private readonly IIngredientService service;
        private readonly IViewModelMapper<IngredientDTO, IngredientViewModel> mapper;
        private readonly IImageStorageProvider imageStorage;
        private readonly IToastNotification toastNotification;

        public IngredientsController(IIngredientService service, IViewModelMapper<IngredientDTO,
            IngredientViewModel> mapper, IImageStorageProvider imageStorage, IToastNotification toastNotification)
        {
            this.service = service;
            this.mapper = mapper;
            this.imageStorage = imageStorage;
            this.toastNotification = toastNotification;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            try
            {
                var ingredients = await this.service.GetAllIngredientsAsync(page);

                var ingredientViews = this.mapper.MapToViewModel(ingredients);

                return View(ingredientViews);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> Details(Guid? id)
        {
            try
            {
                var ingredient = await this.service.GetIngredientAsync(id.Value);

                var ingredientViewModel = this.mapper.MapToViewModel(ingredient);

                return View(ingredientViewModel);

            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] IngredientCreationViewModel creationModel)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    if (creationModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(creationModel.File, "ingredients");
                        creationModel.IngredientViewModel.ImgPath = imagePath;
                    }
                    else
                    {
                        creationModel.IngredientViewModel.ImgPath = ConstantsHolder.DefaultIngredientsImg;
                    }
                    var ingredientDTO = this.mapper.MapToDTO(creationModel.IngredientViewModel);
                    var ingredient = await this.service.CreateIngredientAsync(ingredientDTO);

                    var ingredientViewModel = this.mapper.MapToViewModel(ingredient);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.IngredientCreated);
                    return RedirectToAction("Index");
                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.IngredientExists);
                    return View(creationModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(creationModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(creationModel);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            try
            {

                var ingredient = await this.service.GetIngredientAsync(id.Value);
                var ingredientView = this.mapper.MapToViewModel(ingredient);
                var ingredientEditModel = new IngredientCreationViewModel { IngredientViewModel = ingredientView };

                return View(ingredientEditModel);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind] IngredientCreationViewModel editModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    if (editModel.File != null)
                    {
                        var imagePath = await this.imageStorage.StoreImage(editModel.File, "ingredients");
                        editModel.IngredientViewModel.ImgPath = imagePath;
                    }
                    var ingredientDTO = this.mapper.MapToDTO(editModel.IngredientViewModel);
                    var ingredient = await this.service.EditIngredientAsync(ingredientDTO);
                    editModel.IngredientViewModel = this.mapper.MapToViewModel(ingredient);
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.IngredientEdited);
                    return RedirectToAction(nameof(Index));
                }
                catch (ArgumentException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.IngredientExists);
                    return View(editModel);
                }
                catch (InvalidDataException)
                {
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.InvalidFiletype);
                    return View(editModel);
                }
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(editModel);
        }


        public async Task<IActionResult> Delete(Guid? id)
        {
            try
            {
                var ingredient = await this.service.GetIngredientAsync(id.Value);
                var ingredientView = this.mapper.MapToViewModel(ingredient);
                return View(ingredientView);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var result = await this.service.DeleteIngredientAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.IngredientDeleted);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.IngredientNotDeleted);
            return RedirectToAction(nameof(Index));
        }

    }
}
