﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CocktailMagician.Data;
using CocktailMagician.Data.Models;
using CocktailMagician.Web.ViewModelMappers.Contracts;
using CocktailMagician.Services.DTOs;
using CocktailMagician.Web.Areas.Admin.Models;
using CocktailMagician.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using NToastNotify;
using CocktailMagician.Web.Utilities.Constants;

namespace CocktailMagician.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IUserService service;
        private readonly IViewModelMapper<UserDTO, UserViewModel> mapper;
        private readonly IToastNotification toastNotification;

        public UsersController(IUserService service, IViewModelMapper<UserDTO, UserViewModel> mapper, IToastNotification toastNotification)
        {
            this.service = service;
            this.mapper = mapper;
            this.toastNotification = toastNotification;
        }

        // GET: Admin/Users
        public async Task<IActionResult> Index()
        {
            var userDTOs = await this.service.GetAllUsersAsync();
            var bannedUserDTOs = await this.service.GetBannedUsersAsync();
            var allUsersViewModel = new AllUsersViewModel();
            allUsersViewModel.AllUsers = mapper.MapToViewModel(userDTOs);
            allUsersViewModel.BannedUsers = mapper.MapToViewModel(bannedUserDTOs);
            return View(allUsersViewModel);
        }
        public async Task<IActionResult> Ban(Guid? id)
        {
            try
            {
                var user = await this.service.GetUserAsync(id.Value);
                var userViewModel = this.mapper.MapToViewModel(user);
                return View(userViewModel);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Ban(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                var result = await this.service.BanUserAsync(user.Id, user.BannedUntil);
                if (result)
                    this.toastNotification.AddSuccessToastMessage(ConstantsHolder.UserBanned);
                else
                    this.toastNotification.AddErrorToastMessage(ConstantsHolder.UserNotBanned);
                
                return RedirectToAction("Index");
            }
            this.toastNotification.AddWarningToastMessage(ConstantsHolder.InvalidInput);
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UnBan(Guid id)
        {
            var result = await this.service.UnbanUserAsync(id);
            if (result)
                this.toastNotification.AddSuccessToastMessage(ConstantsHolder.UserUnbanned);
            else
                this.toastNotification.AddErrorToastMessage(ConstantsHolder.UserNotUnbanned);

            return RedirectToAction("Index");
        }
    }
}
