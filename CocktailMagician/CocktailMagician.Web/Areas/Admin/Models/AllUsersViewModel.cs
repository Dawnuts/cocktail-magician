﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocktailMagician.Web.Areas.Admin.Models
{
    public class AllUsersViewModel
    {
        public ICollection<UserViewModel> AllUsers { get; set; }
        public ICollection<UserViewModel> BannedUsers { get; set; }
    }
}
